﻿Projet-spring : Internet Movie DataBase

Objectifs

Les 3 objectifs de ce projet sont les suivants :
1. Réaliser un document de conception avec diagramme de classes et Modèle Physique de Données.
2. Mettre en base de données des informations concernant le cinéma,
3. Mettre au point une API REST permettant d’effectuer des recherches dans les données.

Description

Les données sont réparties dans 5 fichiers CSV :
1. films.csv : liste des films
2. acteurs.csv : liste des acteurs
3. realisateurs.csv : liste des réalisateurs
4. roles.csv : listes des rôles par films
5. film_realisateurs.csv : réalisateurs par films

Conception 

Fichiers MCD.jpg, MPD.jpg et ULM.jpg dans le package conception à la racine du projet 


2 applications Spring Boot :

1. ProjetSpringApplication : qui sera exécutée pour lancer l’API REST.
2. TraitementFichiersApplication : qui sera exécutée pour mettre en base le contenu des fichiers stockés dans resources.

Maquette filaire du site web :

```
https://www.figma.com/file/TtLcaIMEt9XouMvkFlQ300/Untitled?type=design&node-id=0%3A1&mode=design&t=p0Vqk16izynYKMRg-1
```


Mise en place des pages html dynamiques avec javascript :

Réalisation d'un site avec un bandeau de navigation qui permet d’accéder aux différentes
fonctionnalités. Votre site doit permettre de réaliser les actions suivantes:

a. Gestion des genres : lister les genres existants, ajout, modification, suppression  
b. Recherche de tous les films (nom et années de sortie) d’un acteur donné  
c. Modification d’un film  
d. Recherche de tous les rôles d’un film donné  
e. Recherche des films sortis entre 2 années données  
f. Recherche des films communs à 2 acteurs ou actrices donnés.  
g. Affichage de tous les films d’un genre donné  
h. Recherche des acteurs communs à 2 films donnés  
i. Modification d’un acteur  
j. Recherche de tous les films d’un réalisateur donné  
k. Recherche des films sortis entre 2 années données et qui ont un acteur/actrice
donné parmi les acteurs  
l. Recherche des acteurs associés au genre dans lequel ils ont le plus joué  
