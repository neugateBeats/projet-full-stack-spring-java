let btn = document.getElementById("btnSend");
let genreField = document.getElementById("genre");
let dataGenre;

// event listeners
btn.addEventListener("click", handleClick, false);

/*********************************************************************************
 * Fonction hanleClick qui permet de filtrer le genre
 *
 *********************************************************************************/
async function handleClick(e) {
  // grab city value
  let genre = genreField.value;
  console.log(genreField + "," + genreField.value);
  // disable form
  genreField.disabled = true;
  btn.disabled = true;
  // create xhr
  let tbody = document.querySelector("#liste-genres tbody");

  // Clear existing rows
  tbody.innerHTML = "";
  if (genre.length != 0) {
    let filteredData = dataGenre.filter(
     (res) => res.nom.toLowerCase() === genre.toLowerCase()
    );
    console.log(" Filtered data " + filteredData.length);
    if (filteredData.length > 0) {
      filteredData.forEach((data) => {
       console.log(data.idGenre + "," + data.nom);
        let newRow = document.createElement("tr");
        newRow.setAttribute("id", data.idGenre);

        let titreGenre = document.createElement("td");
        titreGenre.textContent = data.nom;
        let buttonCell = document.createElement("td");
        let deleteButton = document.createElement("button");
        //deleteButton.textContent = "Delete";
        // Ajouter la classe "delete-btn" au bouton
        deleteButton.className = "delete-btn";
        // Ajouter la classe "delete-btn" au bouton
        deleteButton.className = "delete-btn d-flex align-items-center justify-content-center";
        // Ajouter l'icône
        let trashIcon = document.createElement("i");
        trashIcon.className = "fas fa-trash-alt";
        trashIcon.style.color = "red";
        deleteButton.appendChild(trashIcon);
        buttonCell.appendChild(deleteButton);
        // Ajouter un gestionnaire d'événements pour le bouton "Delete"
        deleteButton.addEventListener("click", function () {
        // Supprimez la ligne associée lorsque le bouton "Delete" est cliqué
        deleteGenre(genre.idGenre);
        });
        // Ajouter le bouton "Modifier" à la deuxième cellule
        let updateButton = document.createElement("button");
        //updateButton.textContent = "Update";
        updateButton.className = "update-btn";
        // Ajouter la classe "update-btn" au bouton
        updateButton.className = "update-btn d-flex align-items-center justify-content-center";
        // Ajouter l'icône de modification
        let editIcon = document.createElement("i");
        editIcon.className = "fas fa-edit";
        editIcon.style.color = "blue";
        updateButton.appendChild(editIcon);
        buttonCell.appendChild(deleteButton);

        // Ajouter un gestionnaire d'événements pour le bouton "update"
        updateButton.addEventListener("click", function () {
        // Modifier la ligne associée lorsque le bouton "Update" est cliqué
        updateGenre(genre.idGenre);
        });

        buttonCell.appendChild(deleteButton);
        buttonCell.appendChild(updateButton);
        newRow.appendChild(titreGenre);
        newRow.appendChild(buttonCell);
        tbody.appendChild(newRow);



        // enable form after search is complete
      });
    }
  } else {
    dataGenre.forEach((genre) => {
      console.log(genre.idGenre + "," + genre.nom);
      let newRow = document.createElement("tr");
      newRow.setAttribute("id", genre.idGenre);

      let titreGenre = document.createElement("td");
      titreGenre.textContent = genre.nom;

      // Ajouter le bouton "Delete" à la deuxième cellule
      let buttonCell = document.createElement("td");
      let deleteButton = document.createElement("button");
      deleteButton.textContent = "Delete";
      deleteButton.className = "delete-btn";

      // Ajouter un gestionnaire d'événements pour le bouton "Delete"
      deleteButton.addEventListener("click", function () {
        // Supprimez la ligne associée lorsque le bouton "Delete" est cliqué
        deleteGenre(data.idGenre);
      });

      // Ajouter le bouton "Modifier" à la deuxième cellule

      let updateButton = document.createElement("button");
      updateButton.textContent = "Update";
      updateButton.className = "update-btn";

      // Ajouter un gestionnaire d'événements pour le bouton "update"
      updateButton.addEventListener("click", function () {
        // Modifier la ligne associée lorsque le bouton "Update" est cliqué
      updateGenre(data.idGenre);
      });

      buttonCell.appendChild(deleteButton);
      buttonCell.appendChild(updateButton);
      newRow.appendChild(titreGenre);
      newRow.appendChild(buttonCell);
      tbody.appendChild(newRow);
    });
  }
  genreField.disabled = false;
  btn.disabled = false;
  /*
    .catch((error) =>
      console.error("Erreur lors de la récupération des genres :", error)
    );*/
}

/*********************************************************************************
 * Fonction OpenPopup qui permet d'afficher un popup et de d'ajouter un nouveau genre
 *
 *********************************************************************************/
let btnAdd = document.getElementById("btnAdd");

// event listener
btnAdd.addEventListener("click", openPopup, false);

function openPopup() {
    // Utilisez une fenêtre modale (popup) pour saisir le genre
    let newGenre = prompt("Entrez un nouveau genre :");

    // Vérifiez si l'utilisateur a saisi quelque chose
    if (newGenre !== null && newGenre.trim() !== "") {
        // Effectuez votre requête pour ajouter le nouveau genre ici

        let apiUrl = "/genre";
        fetch(apiUrl, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ nom: newGenre }),
        })
        .then((data) => {
            console.log("Nouveau genre inséré :", data);
            // Handle the response as needed
            reload();
            // Fermez la fenêtre modale
           window.alert("Nouveau genre ajouté avec succès!");
        })
        .catch((error) =>
            console.error("Erreur lors de l'insertion du nouveau genre :", error)
        );
    }  {
        // Aucun genre saisi, vous pouvez afficher un message à l'utilisateur si nécessaire
    }
}


/*********************************************************************************
 * fonction reload permet d'afficher tous les genres
 *
 *********************************************************************************/
async function reload() {
  fetch("/genre")
    .then((res) => res.json())
    .then((data) => {
      console.log("Données reçues :", data);
      dataGenre = data;
      let tbody = document.querySelector("#liste-genres tbody");

      tbody.innerHTML = "";
      data.forEach((genre) => {
        console.log(genre.idGenre + "," + genre.nom);
        let newRow = document.createElement("tr");
        newRow.setAttribute("id", genre.idGenre);

        let titreGenre = document.createElement("td");
        titreGenre.textContent = genre.nom;

        // Ajouter le bouton "Delete" à la deuxième cellule
        let buttonCell = document.createElement("td");
        let deleteButton = document.createElement("button");
        //buttonCell.className = "text-center";

        //deleteButton.textContent = "Delete";

        // Ajouter la classe "delete-btn" au bouton
        deleteButton.className = "delete-btn";

        // Ajouter la classe "delete-btn" au bouton
        deleteButton.className = "delete-btn d-flex align-items-center justify-content-center";

        // Ajouter l'icône
        let trashIcon = document.createElement("i");
        trashIcon.className = "fas fa-trash-alt";
        trashIcon.style.color = "red";


        // Ajouter l'icône à l'intérieur du bouton
        deleteButton.appendChild(trashIcon);

        // Ajouter le bouton à la cellule
        buttonCell.appendChild(deleteButton);

        // Ajouter un gestionnaire d'événements pour le bouton "Delete"
        deleteButton.addEventListener("click", function () {
          // Supprimez la ligne associée lorsque le bouton "Delete" est cliqué
          deleteGenre(genre.idGenre);
        });

        // Ajouter le bouton "Modifier" à la deuxième cellule

        let updateButton = document.createElement("button");
        //updateButton.textContent = "Update";
        updateButton.className = "update-btn";

        // Ajouter la classe "update-btn" au bouton
         updateButton.className = "update-btn d-flex align-items-center justify-content-center";

        // Ajouter l'icône de modification
        let editIcon = document.createElement("i");
        editIcon.className = "fas fa-edit";
        editIcon.style.color = "blue";


        // Ajouter l'icône à l'intérieur du bouton
        updateButton.appendChild(editIcon);

        // Ajouter le bouton à la cellule
        buttonCell.appendChild(updateButton);

        // Ajouter un gestionnaire d'événements pour le bouton "update"
        updateButton.addEventListener("click", function () {
          // Modifier la ligne associée lorsque le bouton "Update" est cliqué
        updateGenre(genre.idGenre);
        });

        buttonCell.appendChild(deleteButton);
        buttonCell.appendChild(updateButton);
        newRow.appendChild(titreGenre);
        newRow.appendChild(buttonCell);
        tbody.appendChild(newRow);
      });
    })
    .catch((error) =>
      console.error("Erreur lors de la récupération des genres :", error)
    );
}

/*********************************************************************************
 * fonction deleteGenre permet de supprimer le genre sélectionné
 *
 *********************************************************************************/
async function deleteGenre(genreId) {
    try {
        let confirmDelete = confirm("Voulez-vous vraiment supprimer ce genre ?");

        if (confirmDelete) {
            let apiUrl = "/genre/" + genreId;

            let response = await fetch(apiUrl, {
                method: "DELETE"
            });

            if (!response.ok) {
                throw new Error(`Erreur lors de la suppression du genre (statut ${response.status})`);
            }

            console.log("Genre supprimé avec succès");
            reload();
        } else {
            console.log("Suppression annulée par l'utilisateur");
        }
    } catch (error) {
        console.error("Erreur lors de la suppression du genre :", error);
    }
}
reload();

/*********************************************************************************
 * fonction  showUpdatePopup permet d'afficher le popup pour modifier le genre
 *
 *********************************************************************************/
function showUpdatePopup(genreId, currentName) {
  let newName = prompt("Nouveau nom du genre :", currentName);

  if (newName !== null) {
    // Appeler la fonction de modification avec le nouvel identifiant
    modifyGenre(genreId, newName);
  }
}

/*********************************************************************************
 * fonction updateGenre pour récupérer le genre actuel
 *
 *********************************************************************************/
async function updateGenre(genreId) {
  // Vous pouvez ajouter ici la logique pour récupérer le nom actuel du genre
  let currentName = dataGenre.find(genre => genre.idGenre === genreId).nom;

  // Afficher le popup de modification
  showUpdatePopup(genreId, currentName);
}

/*********************************************************************************
 * fonction modifyGenre pour modifier le genre actuel
 *
 *********************************************************************************/
async function modifyGenre(genreId, newName) {
  let apiUrl = "/genre/" + genreId;

  fetch(apiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ nom: newName }), // Vous devez ajuster cette partie en fonction de votre API
  })
    .then((data) => {
      console.log("Genre modifié :", data);
      // Rechargez le tableau après la modification
      reload();
    })
    .catch((error) =>
      console.error("Erreur lors de la modification du genre :", error)
    );
}
