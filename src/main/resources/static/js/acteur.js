let btnSearchIdImdb = document.getElementById("buttonSearchIdImdb");
let btnSearchActeurNom = document.getElementById("btnSearchActeurNom");
let nomActeurField = document.getElementById("searchActeurNom");
let btnAddActeur = document.getElementById("btnAddActeur");
//let buttonSearchActeursByFilms=document.getElementById("buttonSearchActeursByFilms");
let buttonSearchActeursByGenre = document.getElementById("buttonSearchActeursByGenre");
let btnActFilm = document.getElementById("buttonSearchAct");
let sortStates = {}; // Objet pour suivre l'état du tri pour chaque colonne

let dataActeur;

btnSearchIdImdb.addEventListener("click", handleSearchActeursByIdImdbClick, false);
btnSearchActeurNom.addEventListener("click", handleClickNom, false);
btnAddActeur.addEventListener("click", openPopupAddActeur, false);
buttonSearchActeursByGenre.addEventListener("click", handleSearchActeursParGenreClick, false);

btnActFilm.addEventListener("click", openPopupActFilm, false);

/****************************************************************
 * fonction pour afficher le titre de la recherche de films
 * @param : titre
 ****************************************************************/
function displayTitre(titre) {
    let ttitre = document.querySelector("#titre");

    // Clear existing rows
    ttitre.innerHTML = "";
    document.querySelector("#titre").textContent = titre;
}
/****************************************************************
 * Fonction pour charger et afficher tous les acteurs depuis le backend
 ****************************************************************/
async function reloadActeurs() {
    console.log("reload");
    document.getElementById("spinner").style.display = "block";

    try {
        let apiUrl = "/personne";
        let response = await fetch(apiUrl);

        if (!response.ok) {
            throw new Error(`Erreur lors de la récupération des acteurs (statut ${response.status})`);
        }

        let data = await response.json();
        dataActeur = data;
        titre = "Liste des acteurs";
        displayTitre(titre);
        displayActeurs(data);
    } catch (error) {
        console.error("Erreur lors de la récupération des acteurs :", error);
    }
    document.getElementById("spinner").style.display = "none";

}

/****************************************************************
 * Fonction pour afficher les acteurs dans le tableau HTML
 * @param : data - Les données des acteurs à afficher
 ****************************************************************/
function displayActeurs(data) {

    let tbody = document.querySelector("#liste-acteurs tbody");
    tbody.innerHTML = "";

    const acteursArray = Array.isArray(data) ? data : [data];

    acteursArray.forEach((acteur) => {
        let newRow = document.createElement("tr");

        let idImdbCell = createCellWithLink("https://www.imdb.com/" + acteur.urlActeur, "Blue", acteur.idImdb);
        newRow.appendChild(idImdbCell);

        let nomCell = createTextCell(acteur.identite);
        newRow.appendChild(nomCell);

        let dateNaissanceCell = createTextCell(formatDate(acteur.dateNaissance));
        newRow.appendChild(dateNaissanceCell);

        let lieuNaissanceCell = createTextCell(acteur.lieuNaissance);
        newRow.appendChild(lieuNaissanceCell);

        let tailleCell = createTextCell(acteur.taille || "");
        newRow.appendChild(tailleCell);

        gestionButtonsLignes(newRow, acteur);

        tbody.appendChild(newRow);
    });

   // addSortButtons();
}
/****************************************************************
 * Fonction pour créer une cellule de tableau contenant un lien
 *
 * @param {string} href - L'URL du lien
 * @param {string} color - La couleur du texte du lien
 * @param {string} textContent - Le contenu textuel du lien
 * @returns {HTMLTableCellElement} - La cellule de tableau créée avec le lien
 ****************************************************************/
function createCellWithLink(href, color, textContent) {
    let cell = document.createElement("td");
    let link = document.createElement("a");
    link.href = href;
    link.style.color = color;
    link.innerHTML = textContent;
    cell.appendChild(link);
    return cell;
}
/****************************************************************
 * Fonction pour créer une cellule de tableau contenant du texte
 *
 * @param {string} textContent - Le contenu textuel de la cellule
 * @returns {HTMLTableCellElement} - La cellule de tableau créée avec le texte
 ****************************************************************/
function createTextCell(textContent) {
    let cell = document.createElement("td");
    cell.textContent = textContent;
    return cell;
}
/****************************************************************
 * Fonction pour ajouter des boutons de tri aux en-têtes de colonnes
 *
 * Cette fonction suppose que la
 * dernière colonne de l'en-tête est réservée pour les actions.
 ****************************************************************/
function addSortButtons() {
    let headers = document.querySelectorAll("#liste-acteurs thead th");

    headers.forEach((header, index) => {
        // Vérifier si la colonne n'est pas "Action"
        if (index !== headers.length - 1) {
            let button = document.createElement("button");
            let icon = document.createElement("i");
            icon.classList.add("fas", "fa-sort");
            button.appendChild(icon);

            // Initialiser l'état du tri à 'asc'
            sortStates[index] = 'asc';

            button.addEventListener("click", () => sortColumn(index));
            header.appendChild(button);
        }
    });
}

/****************************************************************
 * Fonction pour trier les lignes d'une table en fonction d'une colonne
 *
 * @param {number} columnIndex - L'index de la colonne à trier
 ****************************************************************/
function sortColumn(columnIndex) {
    let tbody = document.querySelector("#liste-acteurs tbody");
    let rows = Array.from(tbody.querySelectorAll("tr"));

    rows.sort((a, b) => {
        let aValue = a.children[columnIndex].textContent;
        let bValue = b.children[columnIndex].textContent;

        if (sortStates[columnIndex] === 'asc') {
            return aValue.localeCompare(bValue);
        } else {
            return bValue.localeCompare(aValue);
        }
    });

    // Inverser l'état du tri
    sortStates[columnIndex] = (sortStates[columnIndex] === 'asc') ? 'desc' : 'asc';

    tbody.innerHTML = "";
    rows.forEach((row) => tbody.appendChild(row));
}
/********************************************************************
 * fonction handleSearchActeursByIdImdbClick qui permet de chercher un acteur par son idImdb
 *
 ********************************************************************/

async function handleSearchActeursByIdImdbClick() {
    console.log("handleSearchFilmByIdImdbClick");

    let idImdbActeurField = document.getElementById("idImdbActeur");

    // get research value
    let recherche = idImdbActeurField.value.toLowerCase().trim();
    console.log(idImdbActeurField + "," + idImdbActeurField.value);

    // disable form
    idImdbActeurField.disabled = true;
    btnSearchIdImdb.disabled = true;

    // create xhr
    let tbody = document.querySelector("#liste-acteurs tbody");

    // Clear existing rows
    tbody.innerHTML = "";

    if (recherche.length != 0) {
        // on fitre par nom inclus dans recherche
        let filteredData = dataActeur.filter(
            (res) => res.idImdb.toLowerCase().trim() === recherche
        );
        console.log(" Filtered data " + filteredData.length);
        if (filteredData.length != null) {
            // on affiche les films correspondant à la recherche
            titre = "Acteur avec l'idImdb " + recherche;
            displayTitre(titre);
            displayActeurs(filteredData);
        }
    } else {
        // on affiche tous les films
        displayTitre("Liste des acteurs");
        displayActeurs(dataActeur);
    }
    idImdbActeurField.disabled = false;
    btnSearchIdImdb.disabled = false;
}

/****************************************************************
 * Fonction asynchrone pour gérer la recherche d'acteur par nom
 ****************************************************************/
async function handleClickNom() {
    // Assurez-vous que nomActeurField est correctement défini
    console.log("handleClickNom");

    let nomActeurField = document.getElementById("searchActeurNom");

    // Récupérer la valeur du champ de recherche
    let recherche = nomActeurField.value.toLowerCase().trim();
    console.log(nomActeurField + "," + nomActeurField.value);

    // Désactiver le formulaire
    nomActeurField.disabled = true;
    btnAddActeur.disabled = true;

    // Sélectionner le corps du tableau
    let tbody = document.querySelector("#liste-acteurs tbody");

    // Effacer les lignes existantes
    tbody.innerHTML = "";
    if (recherche.length !== 0) {
        // Filtrer les données en fonction de la recherche
        let filteredData = dataActeur.filter((acteur) =>
            acteur.identite.toLowerCase().trim().includes(recherche)
        );
        console.log("Données filtrées : " + filteredData.length);
        // Si des résultats sont trouvés, les afficher
        if (filteredData.length !== 0) {
            titre = "Acteur contenant " + recherche;
            displayTitre(titre);
            displayActeurs(filteredData);
        }
    } else {
            // Si la recherche est vide, afficher tous les acteurs
            displayTitre("Liste des acteurs");
            displayActeurs(dataActeur);

            // Reload acteurs lorsque la zone de recherche est vide
            reloadActeurs();
        }
    nomActeurField.disabled = false;
    btnAddActeur.disabled = false;

}

/**************************************************************************************
 * Fonction pour ouvrir la popup de recherche des acteurs communs à 2
 * à 2 films donnés
 **************************************************************************************/
function openPopupActFilm() {
    console.log("openPopupActFilm");
    // Affichez la popup de recherche es acteurs communs à 2 films donnés
    document.getElementById("actFilmModal").style.display = "block";
}
/**************************************************************************************
 * Fonction pour fermer la popup de recherche des acteurs communs à 2
 * à 2 films donnés
 **************************************************************************************/
function closePopupActFilm() {
        document.getElementById("actFilmModal").style.display = "none";

        // Réinitialiser les champs du formulaire
        document.getElementById("film1Acteur").value = "";
        document.getElementById("film2Acteur").value = "";

        // Réinitialiser les messages d'erreur et les classes d'erreur
        document.getElementById("errorMessageFilm1").textContent = "";
        document.getElementById("errorMessageFilm2").textContent = "";
        document.getElementById("film1Acteur").classList.remove("error");
        document.getElementById("film2Acteur").classList.remove("error");
}

/**************************************************************************************
 * fonction handleSearchActeursByFilmsClick qui permet de chercher des acteurs
 * communs à 2 films donnés
 **************************************************************************************/
async function handleSearchActeursByFilmsClick() {
    console.log("Début de handleSearchActeursByFilmsClick");

    // Récupérer les champs d'entrée pour les films
    let film1ActeurField = document.getElementById("film1Acteur");
    let film2ActeurField = document.getElementById("film2Acteur");
    let errorMessageFilm1 = document.getElementById("errorMessageFilm1");
    let errorMessageFilm2 = document.getElementById("errorMessageFilm2");
    let radioButtons = document.querySelectorAll('input[name="rbActChoix"]');

    // Obtenir les valeurs de recherche
    let film1 = film1ActeurField.value.trim();
    let film2 = film2ActeurField.value.trim();

    console.log("Film 1:", film1);
    console.log("Film 2:", film2);

    // Réinitialiser les messages d'erreur et les classes d'erreur
    errorMessageFilm1.textContent = "";
    errorMessageFilm2.textContent = "";
    film1ActeurField.classList.remove("error");
    film2ActeurField.classList.remove("error");

    // Validation pour le champ Film 1
    if (film1 === "") {
        errorMessageFilm1.textContent = "Veuillez renseigner le champ Film 1.";
        film1ActeurField.classList.add("error");
    }

    // Validation pour le champ Film 2
    if (film2 === "") {
        errorMessageFilm2.textContent = "Veuillez renseigner le champ Film 2.";
        film2ActeurField.classList.add("error");
    }

    // Si l'un des champs est vide, arrêter la fonction
    if (film1 === "" || film2 === "") {
        return;
    }

    // Fermer la modal pour entrer les IDs des acteurs
    closePopupActFilm();

    let tbody = document.querySelector("#liste-acteurs tbody");

    tbody.innerHTML = "";

    let selected;
    for (const radioButton of radioButtons) {
        if (radioButton.checked) {
            selected = radioButton.value;
            break;
        }
    }
    let apiUrl;

    // Créer une requête fetch pour récupérer les acteurs communs
    switch (selected) {
        case "parImdb":
            apiUrl = `/personne/acteur-commun-imdb?idImdbFilm1=${film1}&idImdbFilm2=${film2}`;
            break;
        case "parNom":
            apiUrl = `/personne/acteur-commun-nom?film1=${film1}&film2=${film2}`;
            break;
        default:
            console.log(`Sorry, we are out of ${selected}.`);
            return;
    }

    try {
        let response = await fetch(apiUrl);
        console.log("Réponse de l'API:", response);

        if (!response.ok) {
            throw new Error(`Erreur HTTP: ${response.status}`);
        }

        let data = await response.json();
        closePopupActFilm();
        console.log("Données reçues du backend:", data);

        // Ajouter un titre
        let titre = `Acteurs communs aux films ${film1} et ${film2}`;
        displayTitre(titre);

        // Afficher les acteurs dans le tableau
        displayActeurs(data);
    } catch (error) {
        console.error("Erreur lors de la requête fetch :", error);
    }

    console.log("Fin de handleSearchActeursByFilmsClick");
}

/***************************************************************
 * Fonction asynchrone pour gérer la recherche d'acteurs par le
 * genre le plus joué
 ***************************************************************/

async function handleSearchActeursParGenreClick() {
    console.log("handleSearchActeursParGenreClick");

    // Masquer la section résultats par défaut
    document.getElementById("resultats").style.display = "none";

    // Désactiver le bouton pendant la recherche
    buttonSearchActeursByGenre.disabled = true;

    // Créer une requête fetch pour récupérer les acteurs associés au genre
    try {
        let apiUrl = "/personne/acteurs-avec-genre";
        console.log("URL de l'API:", apiUrl);

        let response = await fetch(apiUrl);
        console.log("Réponse de l'API:", response);

        if (!response.ok) {
            throw new Error(`Erreur HTTP: ${response.status}`);
        }

        let data = await response.json();
        console.log("Données reçues du backend:", data);

        // Afficher les acteurs dans le tableau
        displayActeursByGenre(data);

        // Afficher la section résultats
        document.getElementById("resultats").style.display = "block";
    } catch (error) {
        console.error("Erreur lors de la requête fetch :", error);
    }

    // Réactiver le bouton après la recherche
    buttonSearchActeursByGenre.disabled = false;

    console.log("Fin de handleSearchActeursParGenreClick");
}

/********************************************************
 * Fonction pour afficher les acteurs par genre dans le tableau
 * @param {Object} data - Les données des acteurs à afficher
 ********************************************************/
function displayActeursByGenre(data) {
    let tbody = document.querySelector("#liste-acteurs-genre");
    tbody.innerHTML = "";

    const acteursArray = Array.isArray(data) ? data : [data];

    acteursArray.forEach((acteur) => {
        let newRow = document.createElement("tr");

        let nomActeurCell = document.createElement("td");
        nomActeurCell.textContent = acteur.nomActeur;
        newRow.appendChild(nomActeurCell);

        let genreCell = document.createElement("td");
        genreCell.textContent = acteur.genre;
        newRow.appendChild(genreCell);

        tbody.appendChild(newRow);
    });
}


/****************************************************************
 * Fonction pour ouvrir la popup d'ajout d'acteur
 * **************************************************************/
function openPopupAddActeur() {
    document.getElementById("addActeurModal").style.display = "block";
}

/****************************************************************
 * Fonction pour fermer la popup d'ajout d'acteur
  * *************************************************************/
function closePopupAddActeur() {
    document.getElementById("addActeurModal").style.display = "none";
}

/***************************************************************
 * Fonction asynchrone pour ajouter un nouvel acteur.
 * Récupère les valeurs depuis les champs du formulaire modal,
 * effectue une requête PUT pour insérer le nouvel acteur,
 * puis recharge la liste des acteurs.
 * *************************************************************/
async function addActeur() {
    // Récupérez les valeurs depuis les champs du formulaire modal
    let newActeur = {
        idImdb: document.getElementById("newActeurIdImbd").value,
        identite: document.getElementById("newActeurNom").value,
        dateNaissance: document.getElementById("newActeurDateNaissance").value,
        lieuNaissance: document.getElementById("newActeurLieuNaissance").value,
        taille: document.getElementById("newActeurTaille").value || null,
    };

    try {
        let apiUrl = "/personne";
        let response = await fetch(apiUrl, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(newActeur),
        });

        if (!response.ok) {
            throw new Error(`Erreur lors de l'insertion du nouvel acteur (statut ${response.status}): ${await response.text()}`);
        }

        console.log("Nouvel acteur inséré avec succès");
        closePopupAddActeur();
        reloadActeurs();
    } catch (error) {
        console.error("Erreur lors de l'insertion du nouvel acteur :", error);
    }
}

/****************************************************************
 * Fonction pour formater la date au format 'YYYY-MM-DD'
 * @param : dataString
 ****************************************************************/
function formatDate(dateString) {
    const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
    const formattedDate = new Date(dateString).toLocaleDateString('fr-FR', options).split('/').reverse().join('-');
    return formattedDate;
}


/********************************************************************
 * fonction deleteActeur qui permet de supprimer un acteur par son id
 * @param : personneId
 ********************************************************************/
async function deleteActeur(personneId) {
    try {
        // Affichez la boîte de dialogue de confirmation
        const confirmationDialog = document.getElementById("confirmationDialog");
        confirmationDialog.style.display = "block";

        // Ajoutez des gestionnaires d'événements aux boutons de confirmation
        const confirmDeleteButton = document.getElementById("confirmDeleteButton");
        const cancelDeleteButton = document.getElementById("cancelDeleteButton");

        confirmDeleteButton.addEventListener("click", async () => {
            // Supprimez la boîte de dialogue
            confirmationDialog.style.display = "none";

            // Continuez avec la suppression de l'acteur
            let apiUrl = "/personne/" + personneId;
            let response = await fetch(apiUrl, {
                method: "DELETE",
            });

            if (!response.ok) {
                throw new Error(`Erreur lors de la suppression de l'acteur (statut ${response.status})`);
            }

            console.log("acteur supprimée avec succès");
            reloadActeurs();
        });

        cancelDeleteButton.addEventListener("click", () => {
            // Annulez la suppression et masquez la boîte de dialogue
            console.log("Suppression annulée par l'utilisateur");
            confirmationDialog.style.display = "none";
        });
    } catch (error) {
        console.error("Erreur lors de la suppression de l'acteur :", error);
    }
}

/*****************************************************************************
 * Fonction pour ouvrir la fenêtre modale de mise à jour d'un acteur.
 * Affiche les informations de l'acteur dans le formulaire de mise à jour.
 * @param {Object} acteur - L'objet représentant les informations de l'acteur.
 * ***************************************************************************/
function openUpdatePopupActeur(acteur) {
    console.log("openUpdatePopupActeur:" + acteur.id);

    // Log the values to check if they are correct
    console.log("ID: " + acteur.id);
    console.log("ID Imdb: " + acteur.idImdb);
    console.log("Identite: " + acteur.identite);
    console.log("Date Naissance: " + acteur.dateNaissance);
    console.log("Lieu Naissance: " + acteur.lieuNaissance);
    console.log("Taille: " + acteur.taille);

    document.getElementById("updateActeurId").value = acteur.id;
    document.getElementById("UpdateActeurIdImbd").value = acteur.idImdb;
    document.getElementById("updateActeurNom").value = acteur.identite;
    document.getElementById("updateActeurDateNaissance").value = formatDate(acteur.dateNaissance);
    document.getElementById("updateActeurLieuNaissance").value = acteur.lieuNaissance;
    document.getElementById("updateActeurTaille").value = acteur.taille || "";

    // Add a log statement to check if the modal is displayed
    console.log("Modal Displayed: " + document.getElementById("updateActeurModal").style.display);

    // Affichez la popup de modification d'un acteur
    document.getElementById("updateActeurModal").style.display = "block";
}

/********************************************************************
 * Fonction pour fermer la fenêtre modale de mise à jour d'un acteur.
 * ******************************************************************/
function closeUpdatePopupActeur() {
    console.log("closeUpdatePopupActeur");
    // Fermez la popup de modification d'un acteur
    document.getElementById("updateActeurModal").style.display = "none";
}

/*************************************************************************************************
 * Fonction pour extraire la partie numérique d'une chaîne de caractères représentant la taille.
 * @param {string} heightString - Chaîne de caractères représentant la hauteur (ex. "1.75 m").
 * @returns {number|null} - Partie numérique de la taille si trouvée, sinon null.
 * **********************************************************************************************/
function parseHeight(heightString) {
    // Extract numeric part from the height string using a regular expression
    const matches = heightString.match(/^(\d+(\.\d+)?)\s*m$/i);

    if (matches) {
        // If there is a match, return the parsed numeric part
        return parseFloat(matches[1]);
    } else {
        // If there is no match, return null
        return null;
    }
}

/************************************************************
 * Fonction asynchrone pour la modification d'un acteur.
 * **********************************************************/
async function updateActeur() {
    console.log("updateActeur");

    let acteurToUpdate = {
        id: document.getElementById("updateActeurId").value,
        idImdb: document.getElementById("UpdateActeurIdImbd").value,
        identite: document.getElementById("updateActeurNom").value,
        dateNaissance: document.getElementById("updateActeurDateNaissance").value,
        lieuNaissance: document.getElementById("updateActeurLieuNaissance").value,
        taille: parseHeight(document.getElementById("updateActeurTaille").value) || "",
    };
    //openUpdatePopupActeur(acteurToUpdate)
    let apiUrl = `/personne/${acteurToUpdate.id}`;

    try {
        let response = await fetch(apiUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(acteurToUpdate),
        });

        if (!response.ok) {
            throw new Error(`Erreur lors de la mise à jour de l'acteur (statut ${response.status}): ${await response.text()}`);
        }

        console.log("acteur mis à jour :", response);
        closeUpdatePopupActeur();
        reloadActeurs();
    } catch (error) {
        console.error(`Erreur lors de la mise à jour de l'acteur : ${error.message}`);
    }
}

/********************************************************************************************
 * fonction gestionButtonsLignes qui ajoute les buttons delete et update pour une ligne
 * @param : newRow
 * @param : film
 ********************************************************************************************/
function gestionButtonsLignes(newRow, acteur) {
    // Ajouter une colonne pour les boutons
    let buttonCell = document.createElement("td");

    // Ajouter le bouton "Modifier" avec une icône
    let updateButton = document.createElement("button");
    updateButton.className = "update-btn";

    let editIcon = document.createElement("i");
    editIcon.className = "fas fa-edit";
    editIcon.style.color = "blue";

    // Ajouter l'icône à l'intérieur du bouton
    updateButton.appendChild(editIcon);

    // Ajouter un gestionnaire d'événements pour le bouton "Update"
    updateButton.addEventListener("click", function () {
        // Modifier la ligne associée lorsque le bouton "Update" est cliqué
        openUpdatePopupActeur(acteur);
    });

    // Ajouter le bouton "Delete" avec une icône
    let deleteButton = document.createElement("button");
    deleteButton.className = "delete-btn";

    let trashIcon = document.createElement("i");
    trashIcon.className = "fas fa-trash-alt";
    trashIcon.style.color = "red";


    // Ajouter l'icône à l'intérieur du bouton
    deleteButton.appendChild(trashIcon);

    // Ajouter un gestionnaire d'événements pour le bouton "Delete"
    deleteButton.addEventListener("click", function () {
        // Supprimez la ligne associée lorsque le bouton "Delete" est cliqué
        deleteActeur(acteur.id);
    });

    buttonCell.appendChild(updateButton);
    buttonCell.appendChild(deleteButton);

    newRow.appendChild(buttonCell);
}
addSortButtons();
reloadActeurs();
