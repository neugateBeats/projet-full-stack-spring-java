/****************************************************************
 * Fonction pour formater la date au format 'YYYY'
 * @param : dataString
 ****************************************************************/
function formatDateAnnee(dateString) {
  const options = { year: "numeric" };
  const formattedDate = new Date(dateString).toLocaleDateString(
    "fr-FR",
    options
  );
  return formattedDate;
}
/****************************************************************
 * Fonction pour formater la date au format 'YYYY-MM-DD'
 * @param : dataString
 ****************************************************************/
function formatDate(dateString) {
  const options = { year: "numeric", month: "2-digit", day: "2-digit" };
  const formattedDate = new Date(dateString)
    .toLocaleDateString("fr-FR", options)
    .split("/")
    .reverse()
    .join("-");
  return formattedDate;
}
/****************************************************************
 * Fonction pour ajouter des boutons de tri aux en-têtes de colonnes
 *
 * Cette fonction suppose que la
 * dernière colonne de l'en-tête est réservée pour les actions.
 ****************************************************************/
function addSortButtons(selector) {
  let selectors = selector + " thead th";
  let headers = document.querySelectorAll(selectors);

  headers.forEach((header, index) => {
    // Vérifier si la colonne n'est pas "Action"
    if (index !== headers.length - 1) {
      let button = document.createElement("button");
      let icon = document.createElement("i");
      icon.classList.add("fas", "fa-sort");
      button.appendChild(icon);

      // Initialiser l'état du tri à 'asc'
      sortStates[index] = "asc";

      button.addEventListener("click", () => sortColumn(index, selector));
      header.appendChild(button);
    }
  });
}

/****************************************************************
 * Fonction pour trier les lignes d'une table en fonction d'une colonne
 *
 * @param {number} columnIndex - L'index de la colonne à trier
 ****************************************************************/
function sortColumn(columnIndex, selector) {
  let selectors = selector + " tbody";
  let tbody = document.querySelector(selectors);
  let rows = Array.from(tbody.querySelectorAll("tr"));

  rows.sort((a, b) => {
    let aValue = a.children[columnIndex].textContent;
    let bValue = b.children[columnIndex].textContent;

    if (sortStates[columnIndex] === "asc") {
      return aValue.localeCompare(bValue);
    } else {
      return bValue.localeCompare(aValue);
    }
  });

  // Inverser l'état du tri
  sortStates[columnIndex] = sortStates[columnIndex] === "asc" ? "desc" : "asc";
  tbody.innerHTML = "";
  rows.forEach((row) => tbody.appendChild(row));
}
/****************************************************************
 * fonction pour afficher le titre de la recherche de films
 * @param : titre
 ****************************************************************/
function displayTitre(titre) {
  let ttitre = document.querySelector("#titre");

  // Clear existing rows
  ttitre.innerHTML = "";
  document.querySelector("#titre").textContent = titre;
}
/******************************************************
 *  Fonction de creation d'une cellule de warning
 *  @param htmlFormId
 *  @param message
 *
 * ****************************************************/
function createUpdateWarningCell(htmlFormId, message) {
  if (document.getElementById("warning")) {
    warningCell = document.getElementById("warning");
    warningCell.textContent = message;
  } else {
    let warningCell = document.createElement("p");
    warningCell.setAttribute("id", "warning");
    warningCell.textContent = message;
    warningCell.style.color = "red";
    document.getElementById(htmlFormId).appendChild(warningCell);
  }
}
