let btnSearchIdImdb = document.getElementById("buttonSearchIdImdb");
let btnSearchNom = document.getElementById("buttonSearchNom");
let btnAddFilm = document.getElementById("buttonAdd");
let btnRealFilm = document.getElementById("buttonSearchReal");
let btnAnnActFilm = document.getElementById("buttonSearchAnnAct");
let btnAnnFilm = document.getElementById("buttonSearchAnn");
let btnActFilm = document.getElementById("buttonSearchAct");
let btnGenreFilm = document.getElementById("buttonSearchGenre");
let sortStates = {}; // Objet pour suivre l'état du tri pour chaque colonne

let dataFilm;

// event listeners
btnSearchIdImdb.addEventListener("click", handleSearchFilmByIdImdbClick, false);
btnSearchNom.addEventListener("click", handleSearchFilmByNomClick, false);
btnAddFilm.addEventListener(
  "click",
  () => openPopupFilm("addFilmModal"),
  false
);
btnRealFilm.addEventListener(
  "click",
  () => openPopupFilm("realFilmModal"),
  false
);
btnAnnActFilm.addEventListener(
  "click",
  () => openPopupFilm("annActFilmModal"),
  false
);
btnAnnFilm.addEventListener(
  "click",
  () => openPopupFilm("annFilmModal"),
  false
);
btnActFilm.addEventListener(
  "click",
  () => openPopupFilm("actFilmModal"),
  false
);
btnGenreFilm.addEventListener(
  "click",
  () => openPopupFilm("genreFilmModal"),
  false
);

/***************************************************************************************
 * fonction handleSearchFilmByIdImdbClick qui permet de chercher un film par son idImdb
 *
 ***************************************************************************************/

async function handleSearchFilmByIdImdbClick() {
  console.log("handleSearchFilmByIdImdbClick");

  let idImdbFilmField = document.getElementById("idImdbFilm");

  // get research value
  let recherche = idImdbFilmField.value.toLowerCase().trim();
  console.log("recherche : " + recherche);

  // disable form
  idImdbFilmField.disabled = true;
  btnSearchIdImdb.disabled = true;

  // create xhr
  let tbody = document.querySelector("#liste-Films tbody");

  // Clear existing rows
  tbody.innerHTML = "";

  if (recherche.length != 0) {
    // on fitre par nom inclus dans recherche
    let filteredData = dataFilm.filter(
      (res) => res.idImdb.toLowerCase().trim() === recherche
    );
    console.log(" Filtered data " + filteredData.length);
    if (filteredData.length != null) {
      // on affiche les films correspondant à la recherche
      titre = "Film avec l'idImdb " + recherche;
      displayTitre(titre);
      displayFilms(filteredData);
    }
  } else {
    // on affiche tous les films
    displayTitre("Liste des films");
    displayFilms(dataFilm);
  }
  idImdbFilmField.disabled = false;
  btnSearchIdImdb.disabled = false;
}

/*********************************************************************************
 * fonction handleSearchFilmByNomClick qui permet de chercher un film par son nom
 * contenant la chaine recherchée
 *
 *********************************************************************************/

async function handleSearchFilmByNomClick() {
  console.log("handleSearchFilmByNomClick");
  let nomFilmField = document.getElementById("nomFilm");

  // get research value
  let recherche = nomFilmField.value.toLowerCase().trim();
  console.log("recherche : " + recherche);

  // disable form
  nomFilmField.disabled = true;
  btnSearchNom.disabled = true;

  // create xhr
  let tbody = document.querySelector("#liste-Films tbody");

  // Clear existing rows
  tbody.innerHTML = "";

  if (recherche.length != 0) {
    // on fitre par nom inclus dans recherche
    let filteredData = dataFilm.filter((res) =>
      res.nom.toLowerCase().trim().includes(recherche)
    );
    console.log(" Filtered data " + filteredData.length);
    if (filteredData.length != null) {
      // on affiche les films correspondant à la recherche
      titre = "Film contenant " + recherche;
      displayTitre(titre);
      displayFilms(filteredData);
    }
  } else {
    // on affiche tous les films
    displayTitre("Liste des films");
    displayFilms(dataFilm);
  }
  nomFilmField.disabled = false;
  btnSearchNom.disabled = false;
}
/**************************************************************************************
 * fonction handleSearchFilmByActeursClick qui permet de chercher un film
 * d'un acteur donné (b) ou communs à 2 acteurs (f)
 *
 **************************************************************************************/

async function handleSearchFilmByActeursClick() {
  console.log("handleSearchFilmByActeursClick");

  // Récupérer les champs d'entrée pour les films
  let acteur1FilmField = document.getElementById("acteur1Film");
  let acteur2FilmField = document.getElementById("acteur2Film");
  let radioButtons = document.querySelectorAll('input[name="rbActChoix"]');

  // Obtenir les valeurs de recherche
  let acteur1 = acteur1FilmField.value.trim();
  let acteur2 = acteur2FilmField.value.trim();

  console.log("acteur1 : " + acteur1 + " acteur2 : " + acteur2);

  // create xhr
  let tbody = document.querySelector("#liste-Films tbody");

  // Clear existing rows
  tbody.innerHTML = "";

  let selected;
  for (const radioButton of radioButtons) {
    if (radioButton.checked) {
      selected = radioButton.value;
      break;
    }
  }
  let apiUrl;
  if (acteur1) {
    if (acteur2) {
      //chercher les films communs à 2 acteurs
      switch (selected) {
        case "parImdb":
          apiUrl = `/film/film-commun-imdb?acteurImdb1=${acteur1}&acteurImdb2=${acteur2}`;
          break;
        case "parNom":
          //chercher les films commun à 2 acteurs
          apiUrl = `/film/film-commun-nom?acteurNom1=${acteur1}&acteurNom2=${acteur2}`;
          break;
        default:
          console.log(`Sorry, we are out of ${selected}.`);
      }
      try {
        let response = await fetch(apiUrl);

        if (!response.ok) {
          throw new Error(
            `Erreur lors de la récupération des films communs aux acteurs ${acteur1} et ${acteur2} (statut ${response.status})`
          );
        }
        let data = await response.json();

        closePopupFilm("actFilmModal", "formActFilm");
        titre = `Films commun aux acteurs ${acteur1} et ${acteur2}`;
        displayTitre(titre);
        displayFilms(data);
      } catch (error) {
        console.error(
          `Erreur lors de la récupération des films communs aux acteurs ${acteur1} et ${acteur2}:`,
          error
        );
      }
    } else {
      //chercher un film d'un acteur donné
      switch (selected) {
        case "parImdb":
          apiUrl = `/film/par-acteur-imdb/${acteur1}`;
          titre = `Films de l'acteur ${acteur1}`;
          break;
        case "parNom":
          //chercher les films des acteurs contenant la chaine recherchée
          apiUrl = `/film/acteurs?chaine=${acteur1}`;
          titre = `Films des acteurs contenant ${acteur1}`;
          break;
        default:
          console.log(`Sorry, we are out of ${selected}.`);
      }
      try {
        let response = await fetch(apiUrl);

        if (!response.ok) {
          throw new Error(
            `Erreur lors de la récupération des films de l'acteur ${acteur1} (statut ${response.status})`
          );
        }
        let data = await response.json();

        closePopupFilm("actFilmModal", "formActFilm");
        displayTitre(titre);
        displayFilms(data);
      } catch (error) {
        console.error(
          "Erreur lors de la récupération des films de l'acteur ${acteur1} :",
          error
        );
      }
    }
  } else {
    //message de warning
    createUpdateWarningCell("formActFilm", "L'acteur 1 doit être renseigné !");
  }
}

/**************************************************************************************
 * fonction handleSearchFilmByAnneeClick qui permet de chercher un film
 * entre 2 annees (e)
 *
 **************************************************************************************/

async function handleSearchFilmByAnneeClick() {
  console.log("handleSearchFilmByAnneeClick");
  let anneeDebutFilmField = document.getElementById("anneeDebutFilm");
  let anneeFinFilmField = document.getElementById("anneeFinFilm");

  // get research value
  let debutRecherche = anneeDebutFilmField.value.trim();
  let finRecherche = anneeFinFilmField.value.trim();

  console.log(
    "debutRecherche " + debutRecherche + " finRecherche " + finRecherche
  );

  // create xhr
  let tbody = document.querySelector("#liste-Films tbody");

  // Clear existing rows
  tbody.innerHTML = "";

  if (debutRecherche.length != 0 && finRecherche.length != 0) {
    if (debutRecherche < finRecherche) {
      // les champs sont bien renseignés, on ferme la popup
      closePopupFilm("annFilmModal", "formAnnFilm");
      // on fitre par dates de début et de fin de la recherche
      let filteredData = dataFilm.filter(
        (res) =>
          new Date(res.annee).getFullYear() >= parseInt(debutRecherche) &&
          new Date(res.annee).getFullYear() <= parseInt(finRecherche)
      );

      if (filteredData.length != null) {
        // on affiche les films correspondant à la recherche
        titre = `Films entre ${debutRecherche} et ${finRecherche}`;
        displayTitre(titre);
        displayFilms(filteredData);
      }
    } else {
      //message de warning
      let message = "L'année de début doit être inférieure à l'année de fin !";
      createUpdateWarningCell("formAnnFilm", message);
    }
  } else {
    //message de warning
    let message = "L'année de début et l'année de fin doit être renseignées !";
    createUpdateWarningCell("formAnnFilm", message);
  }
}
/**************************************************************************************
 * fonction handleSearchFilmAnneesActeurClick qui permet de chercher un film
 * entre 2 années données et qui ont un acteur/actrice donné (k)
 *
 **************************************************************************************/

async function handleSearchFilmAnneesActeurClick() {
  console.log("handleSearchFilmAnneesActeurClick");
  let anneeDebutFilmField = document.getElementById("anneeDebutActeurFilm");
  let anneeFinFilmField = document.getElementById("anneeFinActeurFilm");
  let acteurFilmField = document.getElementById("acteurFilm");
  let radioButtons = document.querySelectorAll('input[name="rbAnnActChoix"]');

  // get research value
  let startYear = anneeDebutFilmField.value.trim();
  let endYear = anneeFinFilmField.value.trim();
  let acteur = acteurFilmField.value.trim();

  console.log(
    "Année début = " +
      startYear +
      " année fin = " +
      endYear +
      " acteur = " +
      acteur
  );

  // create xhr
  let tbody = document.querySelector("#liste-Films tbody");

  // Clear existing rows
  tbody.innerHTML = "";

  let selected;
  for (const radioButton of radioButtons) {
    if (radioButton.checked) {
      selected = radioButton.value;
      break;
    }
  }
  let apiUrl;

  if (acteur && startYear.length != 0 && endYear.length != 0) {
    if (startYear < endYear) {
      // les champs sont bien renseignés, on ferme la popup
      closePopupFilm("annActFilmModal", "formAnnActFilm");
      //chercher un film d'un acteur donné entre 2 années
      switch (selected) {
        case "parImdb":
          titre = `Films de l'acteur ${acteur} entre ${startYear} et ${endYear}`;
          apiUrl = `/film/imdb/${acteur}/entre-annees?startYear=${startYear}&endYear=${endYear}`;
          break;
        case "parNom":
          titre = `Films des acteurs contenant ${acteur} entre ${startYear} et ${endYear}`;
          apiUrl = `/film/nom/${acteur}/entre-annees?startYear=${startYear}&endYear=${endYear}`;
          break;
        default:
          console.log(`Sorry, we are out of ${selected}.`);
      }
      try {
        let response = await fetch(apiUrl);

        if (!response.ok) {
          throw new Error(
            `Erreur lors de la récupération des films de l'acteur ${acteur} entre ${startYear} et ${endYear}(statut ${response.status})`
          );
        }
        let data = await response.json();

        displayTitre(titre);
        displayFilms(data);
      } catch (error) {
        console.error(
          `Erreur lors de la récupération des films de l'acteur ${acteur} entre ${startYear} et ${endYear} :`,
          error
        );
      }
    } else {
      //message de warning
      let message = "L'année de début doit être inférieure à l'année de fin !";
      createUpdateWarningCell("formAnnActFilm", message);
    }
  } else {
    //message de warning
    let message =
      "L'acteur, l'année de début et l'année de fin doivent être renseignées !";
    createUpdateWarningCell("formAnnActFilm", message);
  }
}
/**************************************************************************************
 * fonction handleSearchFilmByGenreClick qui permet de chercher un film
 * d'un genre donné (g)
 *
 **************************************************************************************/

async function handleSearchFilmByGenreClick() {
  console.log("handleSearchFilmByGenreClick");
  selectElement = document.querySelector("#genres");
  genre = selectElement.options[selectElement.selectedIndex].value;
  genreNom = selectElement.options[selectElement.selectedIndex].text;

  // create xhr
  let tbody = document.querySelector("#liste-Films tbody");

  // Clear existing rows
  tbody.innerHTML = "";

  if (genre) {
    closePopupFilm("genreFilmModal");
    //chercher tous les films d’un genre donné
    try {
      let apiUrl = `/film/par-genre/${genre}`;

      let response = await fetch(apiUrl);

      if (!response.ok) {
        throw new Error(
          `Erreur lors de la récupération des films du genre ${genre} (statut ${response.status})`
        );
      }
      let data = await response.json();
      let titre = "Films " + genreNom;
      displayTitre(titre);
      displayFilms(data);
    } catch (error) {
      console.error(
        `Erreur lors de la récupération des films du genre ${genre}:`,
        error
      );
    }
  }
}

/**************************************************************************************
 * fonction handleSearchFilmByRealisateurClick qui permet de chercher un film
 * d'un realisateur donné (j)
 *
 **************************************************************************************/

async function handleSearchFilmByRealisateurClick() {
  console.log("handleSearchFilmByRealisateurClick");
  let realisateurFilmField = document.getElementById("realFilmId");
  let radioButtons = document.querySelectorAll('input[name="rbChoix"]');

  // get research value
  let realisateur = realisateurFilmField.value.trim();

  console.log("realisateur " + realisateur);

  // create xhr
  let tbody = document.querySelector("#liste-Films tbody");

  // Clear existing rows
  tbody.innerHTML = "";

  let selected;
  for (const radioButton of radioButtons) {
    if (radioButton.checked) {
      selected = radioButton.value;
      break;
    }
  }

  let apiUrl;
  if (realisateur) {
    closePopupFilm("realFilmModal", "formRealFilm");
    switch (selected) {
      case "parImdb":
        //chercher les films des realisateurs par IdImdb
        apiUrl = `/film/par-realisateur-imdb/${realisateur}`;
        titre = `Films du realisateur ${realisateur}`;
        break;
      case "parNom":
        //chercher les films des realisateurs contenant la chaine recherchée
        apiUrl = `/film/realisateurs?chaine=${realisateur}`;
        titre = `Films des realisateurs contenant ${realisateur}`;
        break;
      default:
        console.log(`Sorry, we are out of ${selected}.`);
    }
    try {
      let response = await fetch(apiUrl);

      if (!response.ok) {
        throw new Error(
          `Erreur lors de la récupération des films du realisateur ${realisateur} (statut ${response.status})`
        );
      }
      let data = await response.json();

      displayTitre(titre);
      displayFilms(data);
    } catch (error) {
      console.error(
        `Erreur lors de la récupération des films du realisateur ${realisateur} :`,
        error
      );
    }
  } else {
    //message de warning
    createUpdateWarningCell(
      "formRealFilm",
      "Le realisateur doit être renseigné !"
    );
  }
}

/********************************************************************
 * fonction deleteFilm qui permet de supprimer un film par son id
 * @param : filmId
 ********************************************************************/
async function deleteFilm(filmId) {
  console.log("deleteFilm");
  try {
    let confirmDelete = confirm("Voulez-vous vraiment supprimer ce film ?");

    if (confirmDelete) {
      let apiUrl = "/film/" + filmId;

      let response = await fetch(apiUrl, {
        method: "DELETE",
      });

      if (!response.ok) {
        throw new Error(
          `Erreur lors de la suppression du film (statut ${response.status})`
        );
      }

      console.log("Film supprimé avec succès");
      reload();
    } else {
      console.log("Suppression annulée par l'utilisateur");
    }
  } catch (error) {
    console.error("Erreur lors de la suppression du film :", error);
  }
}

/*****************************************************************************************
 * fonction rolesFilm qui fait une requète GET qui permet d'obtenir les roles d'un film
 * en fonction de son ID
 * @param film
 *****************************************************************************************/
async function rolesFilm(film) {
  console.log("roleFilm : " + film.id);

  let apiUrl = `/role/par-film/${film.id}`;

  try {
    let apiUrl = `/role/par-film/${film.id}`;
    let response = await fetch(apiUrl);

    if (!response.ok) {
      throw new Error(
        `Erreur lors de la récupération des roles du film ${film.nom} ( ${film.idImdb}) (statut ${response.status})`
      );
    }
    let data = await response.json();

    displayRoleActeur(data, film);
  } catch (error) {
    console.error(
      `Erreur lors de la récupération des roles du film ${film.nom} ( ${film.idImdb}) :`,
      error
    );
  }
}

/*************************************************************************************
 * fonction modifyFilm qui fait une requète POST qui permet de modifier un film
 * en fonction de son ID
 *
 *************************************************************************************/
async function modifyFilm() {
  console.log("modifyFilm");
  // Récupérez les valeurs depuis les champs du formulaire modal
  let modFilm = {
    id: document.getElementById("modFilmId").value,
    idImdb: document.getElementById("modFilmIdImbd").value,
    nom: document.getElementById("modFilmNom").value,
    rating: document.getElementById("modFilmRating").value,
    annee: formatDate(document.getElementById("modFilmAnnee").value),
    resume: document.getElementById("modFilmResume").value,
    langue: document.getElementById("modFilmLangue").value,
    lieuTournage: document.getElementById("modFilmLieuTournage").value,
    pays: document.getElementById("modFilmPays").value,
    url: document.getElementById("modFilmUrl").value || null,
  };
  let apiUrl = `/film/${modFilm.id}`;

  fetch(apiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(modFilm),
  })
    .then((data) => {
      console.log("film modifié :", data);
      closePopupFilm("updateFilmModal");

      // Rechargez le tableau après la modification
      reload();
    })
    .catch((error) =>
      console.error("Erreur lors de la modification du film :", error)
    );
}
/*************************************************************************
 * Fonction addFilm qui fait une requète PUT qui permet d'ajouter un film
 *
 *************************************************************************/
async function addFilm() {
  console.log("addFilm");
  // Récupérez les valeurs depuis les champs du formulaire modal
  let newFilm = {
    idImdb: document.getElementById("newFilmIdImbd").value,
    nom: document.getElementById("newFilmNom").value,
    rating: document.getElementById("newFilmRating").value,
    annee: formatDate(document.getElementById("newFilmAnnee").value),
    resume: document.getElementById("newFilmResume").value,
    langue: document.getElementById("newFilmLangue").value,
    lieuTournage: document.getElementById("newFilmLieuTournage").value,
    pays: document.getElementById("newFilmPays").value,
    url: document.getElementById("newFilmUrl").value || null,
  };
  // verifier si idImdb n'existe pas déjà avant d'ajouter
  if (!dataFilm.find((film) => film.idImdb === newFilm.idImdb)) {
    // Effectuez une requête PUT pour insérer un nouveau film
    try {
      let apiUrl = "/film";
      let response = await fetch(apiUrl, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newFilm),
      });

      if (!response.ok) {
        throw new Error(
          `Erreur lors de l'insertion du nouveau film (statut ${
            response.status
          }): ${await response.text()}`
        );
      }

      console.log("Nouveau film inséré avec succès");
      closePopupFilm("addFilmModal");
      reload();
    } catch (error) {
      console.error("Erreur lors de l'insertion du nouveau film :", error);
    }
  } else {
    console.error("IdImdb " + newFilm.idImdb + " existe déjà");
  }
}

/****************************************************************************
 * Fonction reload qui permet de chercher tous les films et de les afficher
 *
 ****************************************************************************/
async function reload() {
  console.log("reload");

  document.getElementById("spinner").style.display = "block";
  try {
    let apiUrl = "/film";
    let response = await fetch(apiUrl);

    if (!response.ok) {
      throw new Error(
        `Erreur lors de la récupération des films (statut ${response.status})`
      );
    }
    let data = await response.json();
    dataFilm = data;
    titre = "Liste des films";
    displayTitre(titre);
    displayFilms(data);
  } catch (error) {
    console.error("Erreur lors de la récupération des films :", error);
  }
  document.getElementById("spinner").style.display = "none";
}

/****************************************************************
 * Fonction pour afficher les films dans le tableau
 * @param : data
 ****************************************************************/
function displayFilms(data) {
  let tbody = document.querySelector("#liste-Films tbody");

  // Clear existing rows
  tbody.innerHTML = "";

  const filmsArray = Array.isArray(data) ? data : [data];

  filmsArray.forEach((film) => {
    let newRow = document.createElement("tr");

    let idImdbCell = document.createElement("td");
    link = document.createElement("A");
    link.href = "https://www.imdb.com/" + film.url;
    link.style.color = "Blue";
    link.innerHTML = film.idImdb;
    idImdbCell.appendChild(link);
    newRow.appendChild(idImdbCell);

    let nomCell = document.createElement("td");
    nomCell.textContent = film.nom;
    newRow.appendChild(nomCell);

    let ratingCell = document.createElement("td");
    ratingCell.textContent = film.rating;
    newRow.appendChild(ratingCell);

    let anneeCell = document.createElement("td");
    anneeCell.textContent = formatDateAnnee(film.annee);
    newRow.appendChild(anneeCell);

    let resumeCell = document.createElement("td");
    resumeCell.textContent = film.resume;
    newRow.appendChild(resumeCell);

    let langueCell = document.createElement("td");
    langueCell.textContent = film.langue;
    newRow.appendChild(langueCell);

    let lieuTournageCell = document.createElement("td");
    lieuTournageCell.textContent = film.lieuTournage;
    newRow.appendChild(lieuTournageCell);

    let paysCell = document.createElement("td");
    paysCell.textContent = film.pays;
    newRow.appendChild(paysCell);

    gestionButtonsLignes(newRow, film);

    tbody.appendChild(newRow);
  });
}
/*************************************************************
 * Fonction pour afficher les roles d'un film dans le tableau
 * @param {Object} data - Les données des roles à afficher
 *************************************************************/
function displayRoleActeur(data, film) {
  let tbody = document.querySelector("#liste-role-acteur");
  tbody.innerHTML = "";

  let ttitre = document.querySelector("#titre-roles");

  // Clear existing rows
  ttitre.innerHTML = "";
  document.querySelector("#titre-roles").textContent =
    "Roles du film " + film.nom + " (" + film.idImdb + ")";

  const rolesArray = Array.isArray(data) ? data : [data];

  rolesArray.forEach((role) => {
    let newRow = document.createElement("tr");

    let roleCell = document.createElement("td");
    roleCell.textContent = role.personnage;
    newRow.appendChild(roleCell);

    let acteurCell = document.createElement("td");
    acteurCell.textContent = role.nomActeur;
    newRow.appendChild(acteurCell);

    tbody.appendChild(newRow);
  });
}

/********************************************************************************************
 * Fonction gestionButtonsLignes qui ajoute les buttons delete et update pour une ligne
 * @param : newRow
 * @param : film
 ********************************************************************************************/
function gestionButtonsLignes(newRow, film) {
  console.log("gestionButtonsLignes");
  // Ajouter une colonne pour les boutons
  let buttonCell = document.createElement("td");

  // Ajouter le bouton "Delete"
  let deleteButton = document.createElement("button");
  deleteButton.className = "delete-btn";

  // Ajouter l'icône
  let trashIcon = document.createElement("i");
  trashIcon.className = "fas fa-trash-alt";
  trashIcon.style.color = "red";

  // Ajouter l'icône à l'intérieur du bouton
  deleteButton.appendChild(trashIcon); // Ajouter un gestionnaire d'événements pour le bouton "Delete"

  // Ajouter le bouton à la cellule
  buttonCell.appendChild(deleteButton);
  deleteButton.addEventListener("click", function () {
    // Supprimez la ligne associée lorsque le bouton "Delete" est cliqué
    deleteFilm(film.id);
  });

  // Ajouter le bouton "Modifier"
  let updateButton = document.createElement("button");
  updateButton.className = "update-btn";

  // Ajouter l'icône de modification
  let editIcon = document.createElement("i");
  editIcon.className = "fas fa-edit";
  editIcon.style.color = "blue";

  // Ajouter l'icône à l'intérieur du bouton
  updateButton.appendChild(editIcon);

  // Ajouter le bouton à la cellule
  buttonCell.appendChild(updateButton);

  // Ajouter un gestionnaire d'événements pour le bouton "update"
  updateButton.addEventListener("click", function () {
    // Modifier la ligne associée lorsque le bouton "Update" est cliqué
    openPopupUpdateFilm(film);
  });

  // Ajouter le bouton "Roles"
  let roleButton = document.createElement("button");
  roleButton.className = "role-btn";
  roleButton.textContent = "Roles";

  // Ajouter le bouton à la cellule
  buttonCell.appendChild(roleButton);

  // Ajouter un gestionnaire d'événements pour le bouton "Roles"
  roleButton.addEventListener("click", function () {
    // Ouvre une pop up avec les roles de la ligne associée lorsque le bouton "Role" est cliqué
    openPopupRoleFilm(film);
  });

  buttonCell.appendChild(deleteButton);
  buttonCell.appendChild(updateButton);
  buttonCell.appendChild(roleButton);

  newRow.appendChild(buttonCell);
}

/****************************************************************
 * Fonction qui permet de récupèrer les genres pour les afficher
 * dans une combo box
 *  ****************************************************************/
function getGenres() {
  console.log("getGenres");
  let genresArray = [];
  genresArray = fetch("/genre")
    .then((res) => res.json())
    .then((data) => {
      return data;
    });

  var select = document.createElement("select");
  select.name = "genres";
  select.id = "genres";

  genresArray.then((genres) => {
    genres.forEach((genre) => {
      let option = document.createElement("option");

      option.value = genre.idGenre;
      option.text = genre.nom;
      select.appendChild(option);
    });
  });

  var label = document.createElement("label");
  label.innerHTML = "Genre: ";
  label.htmlFor = "genres";

  document.getElementById("container").appendChild(label).appendChild(select);
}
/************************************************************
 *  Fonction openPopupFilm pour ouvrir la popup en parametre
 * @param htlmElementId
 *
 * **********************************************************/
function openPopupFilm(htlmElementId) {
  console.log("openPopupFilm ", htlmElementId);
  // Affichez la popup de recherche
  document.getElementById(htlmElementId).style.display = "block";
}
/******************************************************************************
 *  Fonction openPopupUpdateFilmpour ouvrir la popup de modification d'un film
 * @param: film
 *
 * ****************************************************************************/
function openPopupUpdateFilm(film) {
  console.log("openPopupUpdateFilm:" + film.id);
  // Préparation des données du film à modifier
  document.getElementById("modFilmId").value = film.id;
  document.getElementById("modFilmIdImbd").value = film.idImdb;
  document.getElementById("modFilmNom").value = film.nom;
  document.getElementById("modFilmRating").value = film.rating;
  document.getElementById("modFilmAnnee").value = formatDateAnnee(film.annee);
  document.getElementById("modFilmResume").value = film.resume;
  document.getElementById("modFilmLangue").value = film.langue;
  document.getElementById("modFilmLieuTournage").value = film.lieuTournage;
  document.getElementById("modFilmPays").value = film.pays;
  document.getElementById("modFilmUrl").value = film.url;

  // Affiche la popup de modification d'un film
  document.getElementById("updateFilmModal").style.display = "block";
}
/************************************************************************************
 *  Fonction openPopupRoleFilm pour ouvrir la popup d'affichage des roles d'un film
 * @param: film
 *
 * **********************************************************************************/
function openPopupRoleFilm(film) {
  console.log("openPopupRoleFilm:" + film.id);
  rolesFilm(film);

  // Affichez la popup d'affichage des roles d'un film
  document.getElementById("roleFilmModal").style.display = "block";
}

/**************************************************************
 *  Fonction closePopupFilm pour fermer la popup en parametre
 *  @param htlmElementId
 *  @param htmlFormId
 
 * ***********************************************************/
function closePopupFilm(htmlElementId, htmlFormId) {
  console.log("closePopupFilm :", htmlElementId, ",", htmlFormId);
  // Fermez la popup de recherche des films d'un réalisateur
  document.getElementById(htmlElementId).style.display = "none";
  if (htmlFormId) {
    document.getElementById(htmlFormId).reset();
  }
  if (document.getElementById("warning")) {
    document.getElementById("warning").remove();
  }
}

// init de la page film
addSortButtons("#liste-Films");
reload();
getGenres();
