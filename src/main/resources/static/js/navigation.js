document.addEventListener("DOMContentLoaded", function () {
    // Récupérer tous les liens de la barre de navigation
    const navLinks = document.querySelectorAll('.navbar-nav a');

    // Ajouter un gestionnaire d'événements pour chaque lien
    navLinks.forEach(link => {
        link.addEventListener('click', function () {
            // Retirer la classe 'active' de tous les liens
            navLinks.forEach(navLink => navLink.classList.remove('active'));

            // Ajouter la classe 'active' au lien cliqué
            this.classList.add('active');

            // Appliquer le style directement au lien cliqué
             this.style.color = 'red';
        });
    });
});
