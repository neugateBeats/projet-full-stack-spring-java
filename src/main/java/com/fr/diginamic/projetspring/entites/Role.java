package com.fr.diginamic.projetspring.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_role")
    private int idRole;

    private String personnage;

    @ManyToOne
    @JoinColumn(name = "id_film", nullable = false)

    private Film films;

    @ManyToOne
    @JoinColumn(name = "id_personne", nullable = false)
    private Personne personne;

    public Role() {
    }

    public Role(int idRole, String personnage, Film film, Personne personne) {
        this.idRole = idRole;
        this.personnage = personnage;
        this.films = films;
        this.personne = personne;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return idRole == role.idRole && Objects.equals(personnage, role.personnage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRole, personnage);
    }

    @Override
    public String toString() {
        return "Role{" +
                "idRole=" + idRole +
                ", personnage='" + personnage + '\'' +
                ", film=" + films +
                ", acteur=" + personne +
                '}';
    }

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public String getPersonnage() {
        return personnage;
    }

    public void setPersonnage(String personnage) {
        this.personnage = personnage;
    }

    public Film getFilm() {
        return films;
    }

    public void setFilm(Film film) {
        this.films = film;
    }

    public Personne getActeur() {
        return personne;
    }

    public void setActeur(Personne personne) {
        this.personne = personne;
    }



}



