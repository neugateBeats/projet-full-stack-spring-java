package com.fr.diginamic.projetspring.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "genre")
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_genre")
    private Long idGenre;

    @Column(name = "nom")
    private String nom;

    @ManyToMany(mappedBy = "genres")
    private Set<Film> films;

    public Genre() {
    }

    public Genre(Long idGenre, String nom, Set<Film> films) {
        this.idGenre = idGenre;
        this.nom = nom;
        this.films = films;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "idGenre=" + idGenre +
                ", nom='" + nom + '\'' +
                ", films=" + films +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return Objects.equals(idGenre, genre.idGenre) && Objects.equals(nom, genre.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGenre, nom);
    }

    public Long getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(Long idGenre) {
        this.idGenre = idGenre;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Film> getFilms() {
        return films;
    }

    public void setFilms(Set<Film> films) {
        this.films = films;
    }
}
