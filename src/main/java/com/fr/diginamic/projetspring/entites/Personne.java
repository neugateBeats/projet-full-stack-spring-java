package com.fr.diginamic.projetspring.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
public class Personne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String idImdb;
    @Column
    private String identite;
    @Column
    private Date dateNaissance;
    @Column
    private String lieuNaissance;
    @Column
    String taille;
    @Column
    String urlActeur;
    @Column
    String urlRealisateur;
    @Column(columnDefinition = "BOOL default false")
    Boolean acteur;
    @Column(columnDefinition = "BOOL default false")
    Boolean realisateur;

    @OneToMany(mappedBy = "personne")
    private Set<Role> roles;


    @ManyToMany
    @JoinTable(name = "film_personne",
            joinColumns = @JoinColumn(name = "id_personne", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_film", referencedColumnName = "id"))

    private Set<Film> films;
    public Personne() {
    }

    public Personne(int id, String idImdb, String identite, Date dateNaissance, String lieuNaissance, String taille, String urlActeur, String urlRealisateur, Boolean acteur, Boolean realisateur, Set<Role> roles, Set<Film> films) {
        this.id = id;
        this.idImdb = idImdb;
        this.identite = identite;
        this.dateNaissance = dateNaissance;
        this.lieuNaissance = lieuNaissance;
        this.taille = taille;
        this.urlActeur = urlActeur;
        this.urlRealisateur = urlRealisateur;
        this.acteur = acteur;
        this.realisateur = realisateur;
        this.roles = roles;
        this.films = films;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return Objects.equals(idImdb, personne.idImdb);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idImdb);
    }

    @Override
    public String toString() {
        return "Personne{" +
                "id=" + id +
                ", idImdb='" + idImdb + '\'' +
                ", identite='" + identite + '\'' +
                ", dateNaissance=" + dateNaissance +
                ", lieuNaissance='" + lieuNaissance + '\'' +
                ", taille='" + taille + '\'' +
                ", urlActeur='" + urlActeur + '\'' +
                ", urlRealisateur='" + urlRealisateur + '\'' +
                ", acteur=" + acteur +
                ", realisateur=" + realisateur +
                ", roles=" + roles +
                ", films=" + films +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdImdb() {
        return idImdb;
    }

    public void setIdImdb(String idImdb) {
        this.idImdb = idImdb;
    }

    public String getidentite() {
        return identite;
    }

    public void setidentite(String identite) {
        this.identite = identite;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    public String getUrlActeur() {
        return urlActeur;
    }

    public void setUrlActeur(String url) {
        this.urlActeur = url;
    }
    public String getUrlRealisateur() {
        return urlRealisateur;
    }

    public void setUrlRealisateur(String url) {
        this.urlRealisateur = url;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Boolean getActeur() {
        return acteur;
    }

    public void setActeur(Boolean acteur) {
        this.acteur = acteur;
    }

    public Boolean getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(Boolean realisateur) {
        this.realisateur = realisateur;
    }

    public Set<Film> getFilms() {
        return films;
    }

    public void setFilms(Set<Film> films) {
        this.films = films;
    }
}


