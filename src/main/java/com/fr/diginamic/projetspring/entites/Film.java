package com.fr.diginamic.projetspring.entites;

import jakarta.persistence.*;
import org.hibernate.Length;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String idImdb;
    private String nom;
    private Date annee;
    private float rating;
    private String url;
    private String lieuTournage;
    private String langue;
    @Column(length = 1000)
    private String resume;
    private String pays;

    @ManyToMany
    @JoinTable(
            name = "film_genre",
            joinColumns = @JoinColumn(name = "id_film"),
            inverseJoinColumns = @JoinColumn(name = "id_genre")
    )
    private Set<Genre> genres;


    @ManyToMany
    @JoinTable(
            name = "film_personne",
            joinColumns = @JoinColumn(name = "id_film", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_personne", referencedColumnName = "id"))
    private Set<Personne> personnes;

    @OneToMany(mappedBy = "films")
    private Set<Role> roles;

    public Film() {
    }

    public Film(int id, String idImdb, String nom, Date annee, float rating, String url, String lieuTournage, String langue, String resume, String pays, Set<Genre> genres, Set<Role> roles, Set<Personne> personnes) {
        this.id = id;
        this.idImdb = idImdb;
        this.nom = nom;
        this.annee = annee;
        this.rating = rating;
        this.url = url;
        this.lieuTournage = lieuTournage;
        this.langue = langue;
        this.resume = resume;
        this.pays = pays;
        this.genres = genres;
        this.roles = roles;
        this.personnes = personnes;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", idImdb=" + idImdb +
                ", nom='" + nom + '\'' +
                ", annee=" + annee +
                ", rating=" + rating +
                ", url='" + url + '\'' +
                ", lieuTournage='" + lieuTournage + '\'' +
                ", langue='" + langue + '\'' +
                ", resume='" + resume + '\'' +
                ", pays='" + pays + '\'' +
                ", genres=" + genres +
                ", roles=" + roles +
                ", personnes=" + personnes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Film film = (Film) o;
        return Objects.equals(idImdb, film.idImdb);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idImdb);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdImdb() {
        return idImdb;
    }

    public void setIdImdb(String idImdb) {
        this.idImdb = idImdb;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getAnnee() {
        return annee;
    }

    public void setAnnee(Date annee) {
        this.annee = annee;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLieuTournage() {
        return lieuTournage;
    }

    public void setLieuTournage(String lieuTournage) {
        this.lieuTournage = lieuTournage;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    public Set<Personne> getPersonnes() {
        return personnes;
    }

    public void setPersonnes(Set<Personne> personnes) {
        this.personnes = personnes;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
