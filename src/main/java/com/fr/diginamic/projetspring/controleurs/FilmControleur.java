package com.fr.diginamic.projetspring.controleurs;

import com.fr.diginamic.projetspring.dto.FilmDto;
import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;
import com.fr.diginamic.projetspring.services.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/film")
public class FilmControleur {
    @Autowired
    private FilmService filmService;

    /**
     * méthode GET qui permet de retrouver toutes les films
     *
     * @return Iterable<FilmDto>
     */
    @GetMapping
    public Iterable<FilmDto> getFilms() {
        return filmService.getFilms();
    }


    /**
     * méthode GET qui permet de retrouver un film à partir de son id
     *
     * @param : @PathVariable int id
     * @return : FilmDto
     */
    @GetMapping("/{id}")
    public FilmDto getFilm(@PathVariable int id) {
        return filmService.getFilm(id);

    }

    /**
     * méthode Put qui permet de creer un nouvel film
     *
     * @param : @RequestBody Film nvFilm
     * @return : Film
     * @throws : AnomalieException
     */
    @PutMapping
    public Film inserer(@RequestBody Film nvFilm) throws AnomalieException {
        return filmService.inserer(nvFilm);
    }

    /**
     * méthode Post qui permet de modifier un film
     *
     * @param : @PathVariable int id
     * @param : @RequestBody Film modFilm
     * @return : Film
     * @throws AnomalieException
     **/
    @PostMapping("/{id}")
    public Film modifier(@PathVariable int id, @RequestBody Film modFilm) throws AnomalieException {
        return filmService.modifier(id, modFilm);
    }

    /**
     * méthode DELETE qui supprime une film à partir de son id
     *
     * @param @PathVariable int id
     * @return : Iterable<Film>
     **/

    @DeleteMapping("/{id}")
    public Iterable<Film> deleteFilmById(@PathVariable int id) {
        return filmService.deleteFilmById(id);
    }

    /**
     * 2. méthode GET qui permet d'extraire les films d'un acteur donné par son id
     *
     * @param idPersonne
     * @return  Iterable<FilmDto>
     * @Exemple : GET http://localhost:8080/film/par-acteur/605
     */
    @GetMapping("/par-acteur/{idPersonne}")
    public Iterable<FilmDto> getFilmsByActeurId(@PathVariable int idPersonne) throws AnomalieException {
        return filmService.getFilmsByActeurId(idPersonne);
    }

    /**
     * 2.bis méthode GET qui permet d'extraire les films d'un acteur donné par son idImdb
     *
     * @param acteurIdImdb
     * @return  Iterable<FilmDto>
     * @Exemple : GET http://localhost:8080/film/par-acteur-imdb/nm0000142
     **/
    @GetMapping("/par-acteur-imdb/{acteurIdImdb}")
    public Iterable<FilmDto> getFilmsAUnacteurImdb(@PathVariable String acteurIdImdb) {
        return filmService.getFilmsAUnActeurImdb(acteurIdImdb);
    }

    /**
     * 2.ter méthode GET qui permet d'extraire les films des acteurs contenant une chaine donné
     *
     * @param chaine
     * @return  Iterable<FilmDto>
     * @Exemple  GET http://localhost:8080/film/acteurs?chaine=Clint
     **/
    @GetMapping("/acteurs")
    public Iterable<FilmDto> getFilmsActeursContenantChaine(@RequestParam String chaine) {
        return filmService.getFilmsActeursContenantChaine(chaine);
    }

    /**
     * 4. méthode GET qui permet d'extraire les films sortis entre 2 années données
     *
     * @param startYear
     * @param endYear
     * @return  Iterable<FilmDto>
     * @Exemple : GET http://localhost:8080/film/entre-annees?startYear=1984&endYear=2000
     */
    @GetMapping("/entre-annees")
    public Iterable<FilmDto> getAnneeFilm(@Param("startYear") int startYear, @Param("endYear") int endYear) {
        return filmService.getAnneeFilm(startYear, endYear);
    }

    /**
     * 5. méthode GET qui permet d'extraire les films communs à 2 acteurs ou actrices donnés.
     *
     * @param acteurId1
     * @param acteurId2
     * @return Iterable<FilmDto>
     * @Exemple GET http://localhost:8080/film/film-commun?acteurId1=605&acteurId2=1775
     */
    @GetMapping("/film-commun")
    public Iterable<FilmDto> findFilmCommunA2Acteurs(@RequestParam Integer acteurId1, @RequestParam Integer acteurId2) {
        return filmService.findFilmCommunA2Acteurs(acteurId1, acteurId2);
    }
    /**
     * 5.bis méthode GET qui permet d'extraire les films communs à 2 acteurs donnés par Imdb.
     *
     * @param acteurImdb1
     * @param acteurImdb2
     * @return Iterable<FilmDto>
     * @Exemple GET http://localhost:8080/film/film-commun-imdb?acteurImdb1=nm0787507&acteurImdb2=nm0127408
     */
    @GetMapping("/film-commun-imdb")
    public Iterable<FilmDto> findFilmCommunA2ActeursImdb(@RequestParam String acteurImdb1, @RequestParam String acteurImdb2) {
        return filmService.findFilmCommunA2ActeursImdb(acteurImdb1, acteurImdb2);
    }
    /**
     * 5.ter méthode GET qui permet d'extraire les films communs à 2 acteurs ou actrices donnés.
     *
     * @param acteurNom1
     * @param acteurNom2
     * @return Iterable<FilmDto>
     * @Exemple GET http://localhost:8080/film/film-commun-nom?acteurNom1=Robert Cabal&acteurNom2=Rocky Shahan
     */
    @GetMapping("/film-commun-nom")
    public Iterable<FilmDto> findFilmCommunA2ActeursNom(@RequestParam String acteurNom1, @RequestParam String acteurNom2) {
        return filmService.findFilmCommunA2ActeursNom(acteurNom1, acteurNom2);
    }

    /**
     * 6. méthode GET qui permet d'extraire tous les films d’un genre donné
     *
     * @param  idGenre
     * @return  Iterable<FilmDto>
     * @throws AnomalieException
     * @Exemple : GET http://localhost:8080/film/par-genre/1
     */
    @GetMapping("/par-genre/{idGenre}")
    public Iterable<FilmDto> getFilmsByGenre(@PathVariable Long idGenre) throws AnomalieException {
        return filmService.getFilmsByGenre(idGenre);
    }

    /**
     * 8. méthode GET qui permet d'extraire les films d'un réalisateur donné par son id
     *
     * @param  realisateurId
     * @return : Iterable<FilmDto>
     * @Exemple : GET http://localhost:8080/film/par-realisateur/1303
     **/
    @GetMapping("/par-realisateur/{realisateurIdI}")
    public Iterable<FilmDto> getFilmsAUnRealisateur(@Param("realisateurId") @PathVariable Integer realisateurId) {
        return filmService.getFilmsAUnRealisateur(realisateurId);
    }

    /**
     * 8.bis méthode GET qui permet d'extraire les films d'un réalisateur donné par son idImdb
     *
     * @param realisateurIdImdb
     * @return  Iterable<FilmDto>
     * @Exemple : GET http://localhost:8080/film/par-realisateur-imdb/nm0000142
     **/
    @GetMapping("/par-realisateur-imdb/{realisateurIdImdb}")
    public Iterable<FilmDto> getFilmsAUnRealisateurImdb(@PathVariable String realisateurIdImdb) {
        return filmService.getFilmsAUnRealisateurImdb(realisateurIdImdb);
    }

    /**
     * 8.ter méthode GET qui permet d'extraire les films des réalisateurs contenant une chaine donné
     *
     * @param chaine
     * @return  Iterable<FilmDto>
     * @Exemple  GET http://localhost:8080/film/realisateurs?chaine=Clint
     **/
    @GetMapping("/realisateurs")
    public Iterable<FilmDto> getFilmsRealisateursContenantChaine(@RequestParam String chaine) {
        return filmService.getFilmsRealisateursContenantChaine(chaine);
    }

    /**
     * 9. Extraire les films sortis entre 2 années données et qui ont un acteur/actrice donné parmi les acteurs
     *
     * @param id
     * @param startYear
     * @param endYear
     * @return Iterable<FilmDto>
     * Exemple : GET http://localhost:8080/film/1303/entre-annees?startYear=1984&endYear=2000
     **/
    @GetMapping("/{id}/entre-annees")
    public Iterable<FilmDto> getAnneeFilmByActeur(@PathVariable Integer id, @RequestParam Integer startYear, @RequestParam Integer endYear) {
        return filmService.getAnneeFilmByActeur(id, startYear, endYear);

    }
    /**
     * 9.bis Extraire les films sortis entre 2 années données et qui ont un acteur/actrice donné parmi les acteurs
     *
     * @param acteurImdb
     * @param startYear
     * @param endYear
     * @return Iterable<FilmDto>
     * Exemple : GET http://localhost:8080/film/imdb/nm0000142/entre-annees?startYear=1984&endYear=2000
     **/
    @GetMapping("/imdb/{acteurImdb}/entre-annees")
    public Iterable<FilmDto> getAnneeFilmByActeurImdb(@PathVariable String acteurImdb, @RequestParam Integer startYear, @RequestParam Integer endYear) {
        return filmService.getAnneeFilmByActeurImdb(acteurImdb, startYear, endYear);

    }
    /**
     * 9.ter Extraire les films sortis entre 2 années données et qui ont un acteur/actrice donné parmi les acteurs
     *
     * @param chaine
     * @param startYear
     * @param endYear
     * @return Iterable<FilmDto>
     * Exemple : GET http://localhost:8080/film/nom/clint/entre-annees?startYear=1984&endYear=2000
     **/
    @GetMapping("/nom/{chaine}/entre-annees")
    public Iterable<FilmDto> getAnneeFilmByActeurs(@PathVariable String chaine, @RequestParam Integer startYear, @RequestParam Integer endYear) {
        return filmService.getAnneeFilmByActeurs(chaine, startYear, endYear);

    }
}
