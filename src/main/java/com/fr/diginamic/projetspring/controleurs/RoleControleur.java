package com.fr.diginamic.projetspring.controleurs;

import com.fr.diginamic.projetspring.dto.RoleDto;
import com.fr.diginamic.projetspring.entites.Role;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;
import com.fr.diginamic.projetspring.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/role")
public class RoleControleur {

    @Autowired
    private RoleService roleService;

    /**
     méthode GET qui permet de retrouver toutes les rôles
     @return : Iterable<RoleDto>
     Exemple : GET http://localhost:8080/role
    */
    @GetMapping
    public Iterable<RoleDto> getRoles() {
        return roleService.getRoles();
    }

    /**
     méthode GET qui permet de retrouver un rôle à partir de son id
    @return : RoleDto
    Exemple : GET http://localhost:8080/role/12
    */
    @GetMapping("/{id}")
    public RoleDto getRole(@PathVariable int id) {
        return roleService.getById(id);
    }

    /**
     * méthode GET qui permet de retrouver un rôle par film à partir de son id
     * @param idFilm
     * @return Iterable<RoleDto>
     *  Exemple : GET http://localhost:8080/role/par-film/2
     */

    @GetMapping("/par-film/{idFilm}")
    public Iterable<Map<String, String>> getRolesByFilmId(@PathVariable int idFilm) {
        return roleService.getRolesByFilmId(idFilm);
    }

    /**
     méthode Put qui permet de créer un nouveau rôle
    @param : nvRoleDto
    @return : role
    Exemple : PUT http://localhost:8080/role

    */
    @PutMapping
    public Role inserer(@RequestBody Role nvRole) throws AnomalieException {
        if (nvRole.getPersonnage().length() < 2) {
            throw new AnomalieException("Le rôle doit avoir un nom contenant au moins 2 lettres");
        }
        return roleService.insertRole(nvRole);
    }

    /**
    méthode Post qui permet de modifier un rôle
    @param :  id,  modRoleDto
    @return : role
    Exemple : POST http://localhost:8080/role/11
    */
    @PostMapping("/{id}")
    public RoleDto modifier(@PathVariable int id, @RequestBody RoleDto modRoleDto) throws AnomalieException {
        if (modRoleDto.getPersonnage().length() < 2) {
            throw new AnomalieException("Le rôle doit avoir un nom contenant au moins 2 lettres");
        }
        return roleService.updateRole(id, modRoleDto);
    }

    /**
    méthode DELETE qui supprime un rôle à partir de son id
    @param : @PathVariable int id
    @return : Iterable<RoleDto>
    Exemple : DELETE http://localhost:8080/role/11
    */
    @DeleteMapping("/{id}")
    public Iterable<RoleDto> deleteRoleById(@PathVariable int id) {
        roleService.deleteRole(id);
        return roleService.getRoles();
    }
}
