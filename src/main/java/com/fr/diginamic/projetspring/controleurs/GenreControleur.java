package com.fr.diginamic.projetspring.controleurs;


import com.fr.diginamic.projetspring.dto.GenreDto;
import com.fr.diginamic.projetspring.entites.Genre;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;
import com.fr.diginamic.projetspring.repositories.GenreRepository;
import com.fr.diginamic.projetspring.services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/genre")
public class GenreControleur {
    @Autowired
    private GenreService genreService;

    /**
        méthode GET qui permet de retrouver toutes les genres
        @return : Iterable<GenreDto>
        Exemple : GET http://localhost:8080/genre
     */
    @GetMapping
    public Iterable<GenreDto> getGenres() {

        return genreService.getGenre();

    }

    /**
     méthode GET qui permet de retrouver un genre à partir de son id
     @Param  @PathVariable int id
     @return : GenreDto
     Exemple : GET http://localhost:8080/genre/3
     */
    @GetMapping("/{id}")
    public GenreDto getGenres(@PathVariable int id) {

        return genreService.getGenres(id);

    }


    /**
     méthode Put qui permet de creer un nouvel genre
     @param : @RequestBody Genre nvGenre
     @return : genre
     Exemple : PUT http://localhost:8080/genre
    */
    @PutMapping
    public Genre inserer(@RequestBody Genre nvGenre) throws AnomalieException {

        return genreService.inserer(nvGenre);

    }
    /**
     méthode Post qui permet de modifier un genre
     @param : @PathVariable int id, @RequestBody Genre modGenre
     @return : genre
     Exemple : POST http://localhost:8080/genre/3
    */
    @PostMapping("/{id}")
    public Genre modifier(@PathVariable int id, @RequestBody Genre modGenre) throws AnomalieException {
        return genreService.modifier(id, modGenre);

    }

    /**
     méthode DELETE qui supprime une genre à partir de son id
     @param : @PathVariable int id
     @return : Iterable<Genre>
     Exemple : DELETE http://localhost:8080/genre/3
     */
    @DeleteMapping("/{id}")
    public Iterable<Genre> deleteGenreById(@PathVariable int id) {
        // méthode DELETE qui supprime un genre à partir de son id
        return genreService.deleteGenreById(id);

    }

    @GetMapping("/Genre-par-nom/{nom}")
    public ResponseEntity<String> findNomGenreByNom(@PathVariable String nom) {
        String genreNom = genreService.findNomGenreByNom(nom);
        if (genreNom != null) {
            return ResponseEntity.ok(genreNom);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


}
