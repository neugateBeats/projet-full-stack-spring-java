package com.fr.diginamic.projetspring.controleurs;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @GetMapping("/")
    public String index() {
        return "index.html";
    }

   @GetMapping("/genres")
    public String genre() {
        return "genre.html";}

    @GetMapping("/films")
    public String film() {
        return "film.html";}

    @GetMapping("/roles")
    public String role() {
        return "role.html";}
    @GetMapping("/acteurs")
    public String acteur() {
        return "acteur.html";}


}
