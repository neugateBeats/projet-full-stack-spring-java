package com.fr.diginamic.projetspring.controleurs;

import com.fr.diginamic.projetspring.dto.FilmDto;
import com.fr.diginamic.projetspring.dto.PersonneDto;
import com.fr.diginamic.projetspring.entites.Personne;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;
import com.fr.diginamic.projetspring.services.PersonneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/personne")
public class PersonneControleur {
    @Autowired
    private PersonneService personneService;


    /*
    méthode GET qui permet de retrouver toutes les personnes
        @return : personnes
     */
    @GetMapping
    public Iterable<PersonneDto> getPersonnes() {

        return personneService.getPersonnes();
    }

    /**
     méthode GET qui permet de retrouver un personne à partir de son id
     @return : personneDto
     Exemple : GET http://localhost:8080/personne/7

     */
    @GetMapping("/{id}")
    public PersonneDto getPersonne(@PathVariable int id) {

        return personneService.getPersonne(id);

    }


    /**
     méthode Put qui permet de creer un nouvel personne
     @param : @RequestBody Personne nvPersonne
     @return : personne
     Exemple : PUT http://localhost:8080/personne
    */
    @PutMapping
    public Personne inserer(@RequestBody Personne nvPersonne) throws AnomalieException {

        return personneService.inserer(nvPersonne);

    }
    /**
     méthode Post qui permet de modifier un personne
     @param : @PathVariable int id, @RequestBody Personne modPersonne
     @return : personne
     Exemple : POST http://localhost:8080/personne/2
    */
    @PostMapping("/{id}")
    public Personne modifier(@PathVariable int id, @RequestBody Personne modPersonne) throws AnomalieException {
        return personneService.modifier(id, modPersonne);


    }

    /**
     méthode DELETE qui supprime une personne à partir de son id
     @param : @PathVariable int id
     @return : Iterable<Personne>
     Exemple : DELETE http://localhost:8080/personne/2
     */
    @DeleteMapping("/{id}")
    public Iterable<Personne> deletePersonneById(@PathVariable int id) {

        return personneService.deletePersonneById(id);
    }

    /**
     * methode Get qui permet d'extraire les acteurs à 2 fils données
     * @param : @requestparm inetger idfilm1
     * @param : @requestparam integer idfilm2
     * @return iterable<PersonneDto>
     * http://localhost:8080/personne/acteur-commun?idfilm1=2&idfilm2=4
     */
    @GetMapping("/acteur-commun")
    public Iterable<PersonneDto> getActeurCommunsFilms(@RequestParam Integer idfilm1, @RequestParam Integer idfilm2) {
        return personneService.getActeurCommunsFilms(idfilm1, idfilm2);
    }
    /**
     * 5.bis méthode GET qui permet d'extraire les acteurs communs à 2 films donnés par Imdb.
     *
     * @param idImdbFilm1
     * @param idImdbFilm2
     * @return Iterable<PersonneDto>
     * @Exemple GET http://localhost:8080/personne/acteur-commun-imdb?idImdbFilm1=tt0074130&idImdbFilm2=tt0066194
     */
    @GetMapping("/acteur-commun-imdb")
    public Iterable<PersonneDto> findByActeurCommunsFilmsImdb(@RequestParam String idImdbFilm1, @RequestParam String idImdbFilm2) {
        return personneService.findByActeurCommunsFilmsImdb(idImdbFilm1, idImdbFilm2);
    }
    /**
     * 5.ter méthode GET qui permet d'extraire les acteurs communs à 2 films donnés par leur noms.
     *
     * @param film1
     * @param film2
     * @return Iterable<PersonneDto>
     * @Exemple GET http://localhost:8080/personne/acteur-commun-nom?film1=Les Dobermans reviennent&film2=La vieille garde reprend du service
     */
    @GetMapping("/acteur-commun-nom")
    public Iterable<PersonneDto> findActeurCommunA2FilmsNom(@RequestParam String film1, @RequestParam String film2) {
        return personneService.findActeurCommunA2FilmsNom(film1, film2);
    }
    /**
     * methode Get qui permet d'extraire les acteurs avec le genre dans lequel ils sont le plus joué
     * @return Iterable<Map<String, String>>
     * http://localhost:8080/personne/acteurs-avec-genre
     */
    @GetMapping("/acteurs-avec-genre")
    public Iterable<Map<String, String>> getActorsWithGenre() {
        return personneService.getPersonnesByGenre();
    }


}

