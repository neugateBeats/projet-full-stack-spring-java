package com.fr.diginamic.projetspring;

import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.entites.Genre;
import com.fr.diginamic.projetspring.services.FilmService;
import com.fr.diginamic.projetspring.services.GenreService;
import com.fr.diginamic.projetspring.traitementFichiers.FichierFilm;
import com.fr.diginamic.projetspring.traitementFichiers.FichierFilmRealisateur;
import com.fr.diginamic.projetspring.traitementFichiers.FichierPersonne;
import com.fr.diginamic.projetspring.traitementFichiers.FichierRole;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class TraitementFichiersApplication  {
    @Autowired
    private GenreService genreService;
    @Autowired
    private FilmService filmService;

    @Autowired
    private FichierFilm fichierFilm;
    @Autowired
    private FichierPersonne fichierPersonne;
    @Autowired
    private FichierRole fichierRole;
    @Autowired
    private FichierFilmRealisateur fichierFilmRealisateur;

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(TraitementFichiersApplication.class);
        app.setWebApplicationType(WebApplicationType.NONE);
        ConfigurableApplicationContext context = app.run();
        TraitementFichiersApplication deuxiemeApp = context.getBean(TraitementFichiersApplication.class);
        deuxiemeApp.startImport();
    }


    public void startImport() throws Exception {

        fichierFilm.traitementFichierFilm();
        fichierPersonne.traitementFichierPersonne("acteurs.csv");
        fichierPersonne.traitementFichierPersonne("realisateurs.csv");
        fichierRole.traitementFichierRole();
        fichierFilmRealisateur.traitementFichierFilmRealisateur();


    }

}