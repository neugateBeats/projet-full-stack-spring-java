package com.fr.diginamic.projetspring.repositories;

import com.fr.diginamic.projetspring.entites.Genre;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface GenreRepository extends CrudRepository<Genre, Integer> {

    /**
     * Compte de Genre avec un nom donné
     * @param nom
     * @return int
     **/
    @Query("SELECT count(g) FROM Genre g WHERE g.nom = :nom ")
    int countGenreByName(String nom);

    /**
     * Extraire le Genre avec un nom donné
     * @param nom
     * @return Genre
     */
    @Query("SELECT g FROM Genre g WHERE g.nom = :nom")
    Genre findByName(String nom);

}
