package com.fr.diginamic.projetspring.repositories;

import com.fr.diginamic.projetspring.entites.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoleRepository extends CrudRepository<Role, Integer> {

    /**
     * @param idFilm
     * @return Iterable<Role>
     **/
/*
    @Query("SELECT r FROM Role r WHERE r.films.id = :filmId")
*/
/*
    @Query("SELECT p.identite, p.idImdb, r.personnage, f.id FROM Role r JOIN r.personne p JOIN r.film f")
*/
    @Query("SELECT r.personnage, p.identite FROM Role r JOIN Personne p ON r.personne.id = p.id WHERE r.films.id = :idFilm")
    List<Object[]> getRolesByFilm(@Param("idFilm") int idFilm);

    /**
     * @param filmId
     * @param personneId
     * @return int
     **/
    @Query("SELECT COUNT(r) FROM Role r WHERE r.films.id = :filmId AND r.personne.id = :personneId")
    int countRolesByFilmIdAndPersonneId(int filmId, int personneId);

    /**
     * @param personneId
     * @return Iterable<Role>
     */
    Iterable<Role> getRolesByPersonneId(int personneId);
}