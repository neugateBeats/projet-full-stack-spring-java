package com.fr.diginamic.projetspring.repositories;

import com.fr.diginamic.projetspring.entites.Film;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface FilmRepository extends CrudRepository<Film, Integer>  {
    /**
     * Extraire tous les films (nom et années de sortie) d’un acteur donné
     * @param startYear
     * @param endYear
     * @return Iterable<Film>
     **/
    @Query("SELECT f FROM Film f WHERE YEAR(f.annee) BETWEEN :startYear AND :endYear")
    Iterable<Film> findFilmsBetweenYears(@Param("startYear") int startYear, @Param("endYear") int endYear);

    /**
     * Extraire tous les films d’un réalisateur donné par son id
     * @param realisateurId
     * @return Iterable<Film>
     **/
    @Query("SELECT f FROM Film f JOIN f.personnes p WHERE p.id = :realisateurId")
    Iterable<Film> findFilmsByRealisateur(@Param("realisateurId") @PathVariable Integer realisateurId);

    /**
     * Extraire tous les films d’un réalisateur donné par son idImdb
     * @param realisateurImdb
     * @return Iterable<Film>
     **/
    @Query("SELECT f FROM Film f JOIN f.personnes p WHERE p.idImdb = :realisateurImdb")
    Iterable<Film> findFilmsByRealisateurImdb(String realisateurImdb);

    /**
     * Extraire tous les films des réalisateurs dont le nom contient une chaine donnée
     * @param chaine
     * @return Iterable<Film>
     **/
    @Query("SELECT f FROM Film f JOIN f.personnes p WHERE p.identite LIKE %:chaine%")
    Iterable<Film> findFilmsByRealisateurs(String chaine);


    @Query("SELECT f.nom, f.annee FROM Film f " + "JOIN f.roles r JOIN r.personne p " +"WHERE p.id = :idPersonne AND p.acteur = true")
    List<Object[]> findFilmsByPersonneId(@Param("idPersonne") int idPersonne);

    /**
     * Extraire tous les films d’un genre donné
     * @param : Long idGenre
     * @return Iterable<Film>
     **/
    @Query("SELECT f FROM Film f " + "JOIN f.genres g " + "WHERE g.idGenre = :idGenre")
    Iterable<Film> findFilmsByGenre(@Param("idGenre") Long idGenre);

    /**
     * Extraire tous les films d’un acteur donné par son id
     * @param acteurId
     * @return Iterable<Film>
     **/
    @Query("SELECT f FROM Film f JOIN f.roles r WHERE r.personne.id = :acteurId ")
    Iterable<Film> findFilmsByActeur(Integer acteurId);

    /**
     * Extraire tous les films d’un acteur donné par son idImdb
     * @param acteurImdb
     * @return Iterable<Film>
     **/
    @Query("SELECT f FROM Film f JOIN f.roles r WHERE r.personne.idImdb = :acteurImdb")
    Iterable<Film> findFilmsByActeurImdb(String acteurImdb);

    /**
     * Extraire tous les films des acteurs dont le nom contient une chaine donnée
     * @param chaine
     * @return Iterable<Film>
     **/
    @Query("SELECT f FROM Film f JOIN f.roles r WHERE r.personne.identite LIKE %:chaine%")
    Iterable<Film> findFilmsByActeurs(String chaine);


    /**
     * Extraire les films communs à 2 acteurs ou actrices donnés par leurs id.
     * @param : Integer acteurId1
     * @param : Integer acteurId2
     * @return Iterable<Film>
     **/
     @Query("SELECT DISTINCT f FROM Film f "
            + "JOIN f.roles r1 "
            + "JOIN f.roles r2 "
            + "WHERE r1.personne.id = :acteurId1 "
            + "AND r2.personne.id = :acteurId2 ")
   Iterable<Film> findFilmCommunA2Acteurs(Integer acteurId1, Integer acteurId2);

    /**
     * Extraire les films communs à 2 acteurs ou actrices donnés leurs idimdb.
     * @param : Integer acteurId1
     * @param : Integer acteurId2
     * @return Iterable<Film>
     **/
    @Query("SELECT DISTINCT f FROM Film f "
            + "JOIN f.roles r1 "
            + "JOIN f.roles r2 "
            + "WHERE r1.personne.idImdb = :acteurImdb1 "
            + "AND r2.personne.idImdb = :acteurImdb2 ")
    Iterable<Film> findFilmCommunA2ActeursImdb(String acteurImdb1, String acteurImdb2);

    /**
     * Extraire les films communs à 2 acteurs ou actrices donnés par leurs noms.
     * @param : Integer acteurId1
     * @param : Integer acteurId2
     * @return Iterable<Film>
     **/
    @Query("SELECT DISTINCT f FROM Film f "
            + "JOIN f.roles r1 "
            + "JOIN f.roles r2 "
            + "WHERE r1.personne.identite = :acteurNom1 "
            + "AND r2.personne.identite = :acteurNom2 ")
    Iterable<Film> findFilmCommunA2ActeursNom(String acteurNom1, String acteurNom2);

    /**
     * Extraire les films sortis entre 2 années données et qui ont un acteur/actrice donné par son id
     * @param acteurId
     * @param startYear
     * @param endYear
     * @return
     **/
    @Query("SELECT f FROM Film f JOIN f.roles r WHERE YEAR(f.annee) BETWEEN :startYear AND :endYear AND r.personne.id = :acteurId")
    Iterable<Film> findFilmsByActeurBetweenYears(Integer acteurId, Integer startYear, Integer endYear);
    /**
     * Extraire les films sortis entre 2 années données et qui ont un acteur/actrice donné par son idImdb
     * @param acteurImdb
     * @param startYear
     * @param endYear
     * @return
     **/
    @Query("SELECT f FROM Film f JOIN f.roles r WHERE YEAR(f.annee) BETWEEN :startYear AND :endYear AND r.personne.idImdb = :acteurImdb")
    Iterable<Film> findFilmsByActeurImdbBetweenYears(String acteurImdb, Integer startYear, Integer endYear);

    /**
     * Extraire les films sortis entre 2 années données et qui ont un acteur/actrice dont le nom contient une chaine donnée
     * @param chaine
     * @param startYear
     * @param endYear
     * @return
     **/
    @Query("SELECT f FROM Film f JOIN f.roles r WHERE YEAR(f.annee) BETWEEN :startYear AND :endYear AND r.personne.identite LIKE %:chaine% ")
    Iterable<Film> findFilmsByActeursBetweenYears(String chaine, Integer startYear, Integer endYear);

    /**
     * Compte de Film avec un idImdb donné
     * @param idImdb
     * @return
     **/
    @Query("SELECT count(f) FROM Film f WHERE f.idImdb = :idImdb")
    int countFilmsByIdImdb(String idImdb);

    /**
     * Extraire Film avec un idImdb donné
     * @param idImdb
     * @return Film
     **/

    @Query("SELECT f FROM Film f WHERE f.idImdb = :idImdb")
    Film filmByIdImdb(String idImdb);
    @Query("SELECT f FROM Film f WHERE f.idImdb = :idImdb")
    Iterable<Film> findFilmByIdImdb(String idImdb);


}

