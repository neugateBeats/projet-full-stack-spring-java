package com.fr.diginamic.projetspring.repositories;

import com.fr.diginamic.projetspring.dto.PersonneDto;
import com.fr.diginamic.projetspring.entites.Personne;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PersonneRepository extends CrudRepository<Personne, Integer> {

    /**
     * Extraire les acteurs communs à 2 films donnés
     * @param idFilm1
     * @param idFilm2
     * @return Iterable<Personne>
     **/
    @Query("SELECT DISTINCT p FROM Personne p " +
            "JOIN p.roles r1 " +
            "JOIN p.roles r2 " +
            "WHERE r1.films.id = :idFilm1 " +
            "AND r2.films.id = :idFilm2 " +
            "AND p.acteur = true")

    Iterable<Personne> findByActeurCommunsFilm(@Param("idFilm1") int idFilm1, @Param("idFilm2") int idFilm2);

    /**
     * Extraire les acteurs communs à 2 films donnés par idImdb
     * @param idImdbFilm1
     * @param idImdbFilm2
     * @return Iterable<Personne>
     **/
    @Query("SELECT DISTINCT p FROM Personne p " +
            "JOIN p.roles r1 " +
            "JOIN p.roles r2 " +
            "WHERE r1.films.idImdb = :idImdbFilm1 " +
            "AND r2.films.idImdb = :idImdbFilm2 " +
            "AND p.acteur = true")

    Iterable<Personne> findByActeurCommunsFilmsImdb(@Param("idImdbFilm1") String idImdbFilm1, @Param("idImdbFilm2") String idImdbFilm2);
    /**
     * Extraire les acteurs communs à 2 films donnés par idImdb
     * @param film1
     * @param film2
     * @return Iterable<Personne>
     **/
    @Query("SELECT DISTINCT p FROM Personne p " +
            "JOIN p.roles r1 " +
            "JOIN p.roles r2 " +
            "WHERE r1.films.nom = :film1 " +
            "AND r2.films.nom = :film2 " +
            "AND p.acteur = true")

    Iterable<Personne> findActeurCommunA2FilmsNom(@Param("film1") String film1, @Param("film2") String film2);

    /**
     * Compte de personne avec un idImdb donné
     * @param idImdb
     * @return int countPersonnesByIdImdb
     */
    @Query("SELECT count(p) FROM Personne p WHERE p.idImdb = :idImdb")
    int countPersonnesByIdImdb(String idImdb);



    /**
     * Extraire personne avec un idImdb donné
     * @param idImdb
     * @return Personne personneByIdImdb
     */
    @Query("SELECT p FROM Personne p WHERE p.idImdb = :idImdb")
    Personne personneByIdImdb(String idImdb);


    /**
     * Extraire une personne par son IDImdb
     * @param idImdb
     * @return Iterable<Personne> findPersonneByIdImdb
     */
    @Query("SELECT p FROM Personne p WHERE p.idImdb = :idImdb")
    Iterable<Personne> findPersonneByIdImdb(String idImdb);

    /**
     * Extraire les acteurs associés au genre dans lequel ils ont le plus joué
     * @returnList<Object[]> findPersonnesByGenre
     */
    @Query("SELECT p.identite AS nomActeur, g.nom AS genreAssocie " +
            "FROM Personne p " +
            "JOIN p.roles r " +
            "JOIN r.films f " +
            "JOIN f.genres g " +
            "WHERE p.acteur = true " +
            "GROUP BY p.id, g.idGenre " +
            "HAVING COUNT(g.idGenre) = " +
            "(SELECT COUNT(g2.idGenre) " +
            "FROM Personne p2 " +
            "JOIN p2.roles r2 " +
            "JOIN r2.films f2 " +
            "JOIN f2.genres g2 " +
            "WHERE p2.id = p.id " +
            "GROUP BY g2.idGenre " +
            "ORDER BY COUNT(g2.idGenre) DESC " +
            "LIMIT 1)")
    List<Object[]> findPersonnesByGenre();
}
