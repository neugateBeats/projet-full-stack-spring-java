package com.fr.diginamic.projetspring.dto;

import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.entites.Genre;
import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


public class GenreDto {
    private Long idGenre;
    private String nom;
    Set<Integer>idsFilm;
    public GenreDto() {
    }
    public GenreDto(Genre genre){
        this.idGenre = genre.getIdGenre();
        this.nom = genre.getNom();
        this.idsFilm = new HashSet<>();
        for (Film film:genre.getFilms()) {
            idsFilm.add(film.getId());
        }
    }
    public GenreDto(Long idGenre, String nom) {
        this.idGenre = idGenre;
        this.nom = nom;
    }

    public Long getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(Long idGenre) {
        this.idGenre = idGenre;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Integer> getIdsFilm() {
        return idsFilm;
    }

    public void setIdsFilm(Set<Integer> idsFilm) {
        this.idsFilm = idsFilm;
    }
}
