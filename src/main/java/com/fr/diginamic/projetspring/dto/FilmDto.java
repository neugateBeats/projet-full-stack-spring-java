package com.fr.diginamic.projetspring.dto;

import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.entites.Genre;
import com.fr.diginamic.projetspring.entites.Personne;
import com.fr.diginamic.projetspring.entites.Role;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class FilmDto {
    int id;
    String idImdb;
    String nom;
    Date annee;
    float rating;
    String url;
    String lieuTournage;
    String langue;
    String resume;
    String pays;
    Set<Long> idsGenre;
    Set<Integer> idsPersonne;
    Set<Integer> idsRole;

    public FilmDto() {
    }

    public FilmDto(Film film) {
        this.id = film.getId();
        this.idImdb = film.getIdImdb();
        this.nom = film.getNom();
        this.annee = film.getAnnee();
        this.rating = film.getRating();
        this.url = film.getUrl();
        this.lieuTournage = film.getLieuTournage();
        this.langue = film.getLangue();
        this.resume = film.getResume();
        this.pays = film.getPays();

        this.idsGenre = new HashSet<>();
        if (film.getGenres() != null) {
            for (Genre genre : film.getGenres()) {
                idsGenre.add(genre.getIdGenre());
            }
        }
        this.idsPersonne = new HashSet<>();
        if (film.getPersonnes() != null) {
            for (Personne personne : film.getPersonnes()) {
                idsPersonne.add(personne.getId());
            }
        }
        this.idsRole = new HashSet<>();
        if (film.getRoles() != null) {
            for (Role role : film.getRoles()) {
                idsRole.add(role.getIdRole());
            }
        }


    }

    public FilmDto(int id, String idImdb, String nom, Date annee, float rating, String url, String lieuTournage, String langue, String resume, String pays) {
        this.id = id;
        this.idImdb = idImdb;
        this.nom = nom;
        this.annee = annee;
        this.rating = rating;
        this.url = url;
        this.lieuTournage = lieuTournage;
        this.langue = langue;
        this.resume = resume;
        this.pays = pays;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdImdb() {
        return idImdb;
    }

    public void setIdImdb(String idImdb) {
        this.idImdb = idImdb;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getAnnee() {
        return annee;
    }

    public void setAnnee(Date annee) {
        this.annee = annee;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLieuTournage() {
        return lieuTournage;
    }

    public void setLieuTournage(String lieuTournage) {
        this.lieuTournage = lieuTournage;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public Set<Long> getIdsGenre() {
        return idsGenre;
    }

    public void setIdsGenre(Set<Long> idsGenre) {
        this.idsGenre = idsGenre;
    }

    public Set<Integer> getIdsPersonne() {
        return idsPersonne;
    }

    public void setIdsPersonne(Set<Integer> idsPersonne) {
        this.idsPersonne = idsPersonne;
    }

    public Set<Integer> getIdsRole() {
        return idsRole;
    }

    public void setIdsRole(Set<Integer> idsRole) {
        this.idsRole = idsRole;
    }
}
