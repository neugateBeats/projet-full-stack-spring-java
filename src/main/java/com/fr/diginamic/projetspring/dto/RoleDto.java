package com.fr.diginamic.projetspring.dto;

import com.fr.diginamic.projetspring.entites.Role;

public class RoleDto {
    private int idRole;
    private String personnage;
    private int filmId;
    private int personneId;

    public RoleDto(Object[] role) {
    }

    public RoleDto(Role role) {
        this.idRole = role.getIdRole();
        this.personnage = role.getPersonnage();
        this.filmId = role.getFilm().getId();
        this.personneId = role.getActeur().getId();
    }

    public RoleDto(int idRole, String personnage, int filmId, int personneId) {
        this.idRole = idRole;
        this.personnage = personnage;
        this.filmId = filmId;
        this.personneId = personneId;
    }


    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public String getPersonnage() {
        return personnage;
    }

    public void setPersonnage(String personnage) {
        this.personnage = personnage;
    }

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public int getPersonneId() {
        return personneId;
    }

    public void setPersonneId(int personneId) {
        this.personneId = personneId;
    }
}
