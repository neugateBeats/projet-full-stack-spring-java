package com.fr.diginamic.projetspring.dto;

import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.entites.Personne;
import com.fr.diginamic.projetspring.entites.Role;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class PersonneDto {
    private int id;
    private String idImdb;
    private String identite;
    private Date dateNaissance;
    private String lieuNaissance;
    private String taille;
    private String urlActeur;
    private String urlRealisateur;
    private Boolean acteur;
    private Boolean realisateur;
    private Set<Integer> rolesIds;
    private Set<Integer> filmsIds;

    public PersonneDto() {
    }
    public PersonneDto(Personne personne) {
                this.id = personne.getId();
                this.idImdb = personne.getIdImdb();
                this.identite = personne.getidentite();
                this.dateNaissance = personne.getDateNaissance();
                this.lieuNaissance = personne.getLieuNaissance();
                this.taille = personne.getTaille();
                this.urlActeur = personne.getUrlActeur();
                this.urlRealisateur = personne.getUrlRealisateur();
                this.acteur = personne.getActeur();
                this.realisateur = personne.getRealisateur();

        this.filmsIds = new HashSet<>();
          for(Film film : personne.getFilms())  {
              filmsIds.add(film.getId());
          }
        this.rolesIds = new HashSet<>();
        for(Role role : personne.getRoles())  {
            filmsIds.add(role.getIdRole());
        }

    }

    public PersonneDto(String identite) {
        this.identite = identite;
    }


    // Constructeur avec les champs principaux
    public PersonneDto(int id, String idImdb, String identite, Date dateNaissance, String lieuNaissance, String taille, String urlActeur, String urlRealisateur, Boolean acteur, Boolean realisateur, Set<Integer> rolesIds, Set<Integer> filmsIds) {
        this.id = id;
        this.idImdb = idImdb;
        this.identite = identite;
        this.dateNaissance = dateNaissance;
        this.lieuNaissance = lieuNaissance;
        this.taille = taille;
        this.urlActeur = urlActeur;
        this.urlRealisateur = urlRealisateur;
        this.acteur = acteur;
        this.realisateur = realisateur;
        this.rolesIds = rolesIds;
        this.filmsIds = filmsIds;
    }

    // Getters et setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdImdb() {
        return idImdb;
    }

    public void setIdImdb(String idImdb) {
        this.idImdb = idImdb;
    }

    public String getIdentite() {
        return identite;
    }

    public void setIdentite(String identite) {
        this.identite = identite;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    public String getUrlActeur() {
        return urlActeur;
    }

    public void setUrlActeur(String urlActeur) {
        this.urlActeur = urlActeur;
    }

    public String getUrlRealisateur() {
        return urlRealisateur;
    }

    public void setUrlRealisateur(String urlRealisateur) {
        this.urlRealisateur = urlRealisateur;
    }

    public Boolean getActeur() {
        return acteur;
    }

    public void setActeur(Boolean acteur) {
        this.acteur = acteur;
    }

    public Boolean getRealisateur() {
        return realisateur;
    }

    public void setRealisateur(Boolean realisateur) {
        this.realisateur = realisateur;
    }

    public Set<Integer> getRolesIds() {
        return rolesIds;
    }

    public void setRolesIds(Set<Integer> rolesIds) {
        this.rolesIds = rolesIds;
    }

    public Set<Integer> getFilmsIds() {
        return filmsIds;
    }

    public void setFilmsIds(Set<Integer> filmsIds) {
        this.filmsIds = filmsIds;
    }




}
