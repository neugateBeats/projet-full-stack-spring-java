package com.fr.diginamic.projetspring.services;

import com.fr.diginamic.projetspring.dto.GenreDto;
import com.fr.diginamic.projetspring.entites.Genre;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;
import com.fr.diginamic.projetspring.repositories.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GenreServiceImpl implements GenreService {
    @Autowired
    private GenreRepository genreRepository;

    /**
    * méthode GET qui permet de retrouver toutes les genres
    *    @return : genres
    **/
    public Iterable<GenreDto> getGenre() {

        Iterable<Genre> genres = genreRepository.findAll();
        List<GenreDto> dtos = new ArrayList<>();
        for (Genre genre:genres) {
            dtos.add(new GenreDto(genre));

        }
        return dtos;
    }

    /**
    * méthode GET qui permet de retrouver un genre à partir de son id
    *    @return : genre
    **/
    public GenreDto getGenres(int id) {
        Optional<Genre> extractGenre = Optional.of(genreRepository.findById(id).get());
        Genre genre = null;
        if(extractGenre.isPresent()){
            genre = extractGenre.get();
        }
        GenreDto dto = new GenreDto(genre);
        return dto;

    }
    /**
    * méthode GET qui permet de compter un genre à partir de son nom
    * @return : int
    **/
    public int countGenreByName(String nom) {

        return genreRepository.countGenreByName(nom);

    }

    /**
    * méthode GET qui permet de retrouver un genre à partir de son nom
    *    @return : genre
    **/
    public Genre findByName(String nom) {
        return genreRepository.findByName(nom);
    }


    /**
     *  méthode Put qui permet de creer un nouvel genre
     * @param @RequestBody Genre nvGenre
     * @return Genre
     * @throws AnomalieException
     */
    public Genre inserer(Genre nvGenre) throws AnomalieException {

        if (nvGenre.getNom().length() < 2) {
            throw new AnomalieException("le genre doit avoir un nom contenant au moins 2 lettres");
        }

        return genreRepository.save(nvGenre);

    }

    /**
    * méthode Post qui permet de modifier un genre
    *    @param : @PathVariable int id, @RequestBody Genre modGenre
    *    @return : genre
    **/
    @PostMapping("/{id}")
    public Genre modifier(int id, Genre modGenre) throws AnomalieException {

        if (modGenre.getNom().length() < 2) {
            throw new AnomalieException("L'acteur doit avoir un nom contenant au moins 2 lettres");
        }
        Genre genreAModifie = genreRepository.findById(id).get();

        genreAModifie.setNom(modGenre.getNom());


        return genreRepository.save(genreAModifie);

    }

    /**
     * méthode DELETE qui supprime une genre à partir de son id
     *    @param : @PathVariable int id
     *    @return : Iterable<Genre>
     **/
    public Iterable<Genre> deleteGenreById(int id) {
        // méthode DELETE qui supprime un genre à partir de son id
        genreRepository.deleteById(id);

        Iterable<Genre> genres = genreRepository.findAll();
        return genres;
    }
    @Override
    public String findNomGenreByNom(String nom) {
        Genre genre = genreRepository.findByName(nom);
        return (genre != null) ? genre.getNom() : null;
    }

}