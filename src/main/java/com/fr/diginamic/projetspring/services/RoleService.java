package com.fr.diginamic.projetspring.services;


import com.fr.diginamic.projetspring.dto.RoleDto;
import com.fr.diginamic.projetspring.entites.Role;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;

import java.util.Map;


public interface RoleService {
    /******************  CRUD  ******************/

    RoleDto getById(int id);

    Iterable<RoleDto> getRoles();

    Role insertRole(Role nvRole) throws AnomalieException;

    RoleDto updateRole(int id, RoleDto roleDto) throws AnomalieException;

    void deleteRole(int id);

    /******************  Autres services  ******************/

    Iterable<RoleDto> getRolesByPersonneId(int personneId);

    Iterable<Map<String, String>> getRolesByFilmId(int idFilm);

    int countRolesByFilmIdAndPersonneId(int filmId, int personneId);
}

