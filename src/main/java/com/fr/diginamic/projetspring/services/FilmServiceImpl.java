package com.fr.diginamic.projetspring.services;

import com.fr.diginamic.projetspring.dto.FilmDto;
import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;
import com.fr.diginamic.projetspring.repositories.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmRepository filmRepository;

    /******************  CRUD  ******************/

    /**
    *
    *    méthode qui permet de retrouver toutes les films
    *    @return : films
    **/
    public Iterable<FilmDto> getFilms() {

        Iterable<Film> films = filmRepository.findAll();

        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
    *    méthode qui permet de retrouver un film à partir de son id
    *    @param : int id
    *    @return : film
    **/
    public FilmDto getFilm(int id) {

        Optional<Film> extractFilm = Optional.of(filmRepository.findById(id).get());
        Film film = null;
        if (extractFilm.isPresent()){
            film = extractFilm.get();
        }
        FilmDto dto = new FilmDto(film);
        return dto;
    }

    @Override
    public Iterable<Film> getFilmImdb(String idImdb) {
        return filmRepository.findFilmByIdImdb(idImdb);
    }

    /**
    *    méthode qui permet de creer un nouvel film
    *    @param : Film nvFilm
    *    @return : film
    **/
    public Film inserer(Film nvFilm) throws AnomalieException {

        if (nvFilm.getIdImdb().isEmpty()) {
            throw new AnomalieException("Le film doit avoir un Id Imdb");
        }

        return filmRepository.save(nvFilm);

    }

    /**
    * méthode qui permet de modifier un film
    *   @param : int id, Film modFilm
    *   @return : film
    **/
    public Film modifier(int id, Film modFilm) throws AnomalieException {

        if (modFilm.getNom().length() < 1) {
            throw new AnomalieException("Le film doit avoir un nom contenant au moins 1 lettres");
        }
        Film filmAModifie = filmRepository.findById(id).get();

        filmAModifie.setIdImdb(modFilm.getIdImdb());
        filmAModifie.setNom(modFilm.getNom());
        filmAModifie.setAnnee(modFilm.getAnnee());
        filmAModifie.setRating(modFilm.getRating());
        filmAModifie.setUrl(modFilm.getUrl());
        filmAModifie.setLieuTournage(modFilm.getLieuTournage());
        filmAModifie.setLangue(modFilm.getLangue());
        filmAModifie.setResume(modFilm.getResume());
        filmAModifie.setPays(modFilm.getPays());

        filmAModifie.setGenres(modFilm.getGenres());
        filmAModifie.setPersonnes(modFilm.getPersonnes());
        filmAModifie.setRoles(modFilm.getRoles());

        return filmRepository.save(filmAModifie);

    }

    /**
    *    méthode qui supprime une film à partir de son id
    *    @param : int id
    *    @return : Iterable<Film>
    **/
    public Iterable<Film> deleteFilmById(int id) {
        filmRepository.deleteById(id);

        Iterable<Film> Films = filmRepository.findAll();
        return Films;
    }
    /******************  Autres services  ******************/

    /**
     *   méthode qui permet de compter les films qui ont le même Id Imdb
     *   @return : int
    **/
    public int countFilmsByIdImdb(String idImdb) {
        return filmRepository.countFilmsByIdImdb(idImdb);
    }
    //Extraire Film avec un idImdb donné

    public Film filmByIdImdb(String idImdb) { return filmRepository.filmByIdImdb(idImdb);}

    /**
    *    2. méthode qui extrait tous les films (nom et années de sortie) d’un acteur donné
    *    @param : int idPersonne
    *    @return : Iterable<Film>
    **/
    public Iterable<FilmDto> getFilmsByActeurId(int idPersonne) throws AnomalieException {
        List<Object[]> result = filmRepository.findFilmsByPersonneId(idPersonne);
        List<Film> films = new ArrayList<>();

        for (Object[] row : result) {
            String nom = (String) row[0];
            Date annee = (Date) row[1];

            Film film = new Film();
            film.setNom(nom);
            film.setAnnee(annee);
            films.add(film);

            if (nom.length() < 2) {
                throw new AnomalieException("Le film doit avoir un nom contenant au moins 2 lettres");
            }
        }
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }

    /**
    *   4. méthode qui permet d'extraire les films sortis entre 2 années données
    *    @param : int startYear, int endYear
    *    @return : films
    **/
    public Iterable<FilmDto> getAnneeFilm(int startYear, int endYear) {
        Iterable<Film> films = filmRepository.findFilmsBetweenYears(startYear, endYear);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }

    /**
    *    5. méthode qui permet d'extraire les films communs à 2 acteurs ou actrices donnés par Id.
    *     @param acteurId1
     *    @param acteurId2
    *     @return : films
    **/
    public Iterable<FilmDto> findFilmCommunA2Acteurs(Integer acteurId1, Integer acteurId2) {

        Iterable<Film> films = filmRepository.findFilmCommunA2Acteurs(acteurId1, acteurId2);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
     *    5.bis méthode qui permet d'extraire les films communs à 2 acteurs ou actrices donnés par IdImdb.
     *     @param acteurImdb1
     *     @param acteurImdb2
     *     @return : films
     **/
    public Iterable<FilmDto> findFilmCommunA2ActeursImdb(String acteurImdb1, String acteurImdb2) {

        Iterable<Film> films = filmRepository.findFilmCommunA2ActeursImdb(acteurImdb1, acteurImdb2);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
     *    5.ter méthode qui permet d'extraire les films communs à 2 acteurs ou actrices donnés par nom.
     *     @param  acteurNom1
     *     @param  acteurNom2
     *     @return : films
     **/
    public Iterable<FilmDto> findFilmCommunA2ActeursNom(String acteurNom1, String acteurNom2) {

        Iterable<Film> films = filmRepository.findFilmCommunA2ActeursNom(acteurNom1, acteurNom2);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }


    /**
    *    6. méthode qui extrait tous les films d’un genre donné
    *    @param : int idGenre
    *    @return : Iterable<Film>
    **/
    public Iterable<FilmDto> getFilmsByGenre(Long idGenre) throws AnomalieException {
        Iterable<Film> films = filmRepository.findFilmsByGenre(idGenre);
        for (Film film : films) {
            if (film.getNom().length() <= 0) {
                throw new AnomalieException("Aucun film trouvé pour le genre avec l'ID : " + idGenre);
            }
        }
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }

    /**
    *   8. méthode qui permet d'extraire les films d'un réalisateur donné par son id
    *   @return : films
    **/
    public Iterable<FilmDto> getFilmsAUnRealisateur(Integer realisateurId) {
        Iterable<Film> films = filmRepository.findFilmsByRealisateur(realisateurId);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
     *   8.bis méthode qui permet d'extraire les films d'un réalisateur donné par son idImdb
     *   @return : films
     **/
    public Iterable<FilmDto> getFilmsAUnRealisateurImdb(String realisateurImdb) {
        Iterable<Film> films = filmRepository.findFilmsByRealisateurImdb(realisateurImdb);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
     *   8.ter méthode qui permet d'extraire tous les films des réalisateurs dont le nom contient une chaine donnée
     *   @return : films
     **/
    public Iterable<FilmDto> getFilmsRealisateursContenantChaine(String chaine) {
        Iterable<Film> films = filmRepository.findFilmsByRealisateurs(chaine);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
    *    9. Extraire les films sortis entre 2 années données et qui ont un acteur/actrice donné par id
     *    @param id
     *    @param startYear
     *    @param endYear
     *    @return films
    **/
    public Iterable<FilmDto> getAnneeFilmByActeur(Integer id, Integer startYear, Integer endYear) {
        Iterable<Film> films = filmRepository.findFilmsByActeurBetweenYears(id, startYear, endYear);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
     *    9.bis Extraire les films sortis entre 2 années données et qui ont un acteur donné par idImdb
     *    @param acteurImdb
     *    @param startYear
     *    @param endYear
     *    @return films
     **/
    public Iterable<FilmDto> getAnneeFilmByActeurImdb(String acteurImdb, Integer startYear, Integer endYear) {
        Iterable<Film> films = filmRepository.findFilmsByActeurImdbBetweenYears(acteurImdb, startYear, endYear);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
     *    9.ter Extraire les films sortis entre 2 années données et qui ont un acteur dont le nom contient une chaine donnée
     *    @param chaine
     *    @param startYear
     *    @param endYear
     *    @return films
     **/
    public Iterable<FilmDto> getAnneeFilmByActeurs(String chaine, Integer startYear, Integer endYear) {
        Iterable<Film> films = filmRepository.findFilmsByActeursBetweenYears(chaine,  startYear, endYear);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }

    /**
    *   méthode qui permet d'extraire les films d'un acteur donné par son idImdb
    *   @param acteurId
     *  @return films
    **/
    public Iterable<FilmDto> getFilmsAUnActeur(Integer acteurId) {
        Iterable<Film> films = filmRepository.findFilmsByActeur(acteurId);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
     *   8.bis méthode qui permet d'extraire les films d'un réalisateur donné par son idImdb
     *   @param acteurImdb
     *   @return : films
     **/
    public Iterable<FilmDto> getFilmsAUnActeurImdb(String acteurImdb) {
        Iterable<Film> films = filmRepository.findFilmsByActeurImdb(acteurImdb);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
    /**
     *   8.ter méthode qui permet d'extraire tous les films des réalisateurs dont le nom contient une chaine donnée
     *   @param chaine
     *   @return : films
     **/
    public Iterable<FilmDto> getFilmsActeursContenantChaine(String chaine) {
        Iterable<Film> films = filmRepository.findFilmsByActeurs(chaine);
        List <FilmDto> dtos = new ArrayList<>();
        for (Film film : films) {
            dtos.add(new FilmDto(film));
        }
        return dtos;
    }
}

