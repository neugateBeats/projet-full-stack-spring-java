package com.fr.diginamic.projetspring.services;

import com.fr.diginamic.projetspring.dto.GenreDto;
import com.fr.diginamic.projetspring.entites.Genre;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;

public interface GenreService {
    Iterable<GenreDto> getGenre();
    GenreDto getGenres(int id);
    int countGenreByName(String nom);
    Genre findByName(String nom);



    Genre inserer(Genre nvGenre) throws AnomalieException;

    Genre modifier(int id, Genre modGenre) throws AnomalieException;

    Iterable<Genre> deleteGenreById(int id);
    String findNomGenreByNom(String nom);
}
