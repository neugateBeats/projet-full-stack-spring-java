package com.fr.diginamic.projetspring.services;

import com.fr.diginamic.projetspring.dto.RoleDto;
import com.fr.diginamic.projetspring.entites.Role;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;
import com.fr.diginamic.projetspring.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    /**
     * méthode GET qui permet de retrouver toutes les roles
     * @Param id
     * @return RoleDto
     */
    @Override
    public RoleDto getById(int id) {
        Role role = roleRepository.findById(id).orElse(null);

        if (role != null) {
            return new RoleDto(role);
        }

        return null;
    }
    /**
     * méthode GET qui permet de retrouver toutes les Role
     * @return Iterable<RoleDto>
     */

    @Override
    public Iterable<RoleDto> getRoles() {
        List<RoleDto> roleDtos = new ArrayList<>();
        Iterable<Role> roles = roleRepository.findAll();

        for (Role role : roles) {
            roleDtos.add(new RoleDto(role));
        }

        return roleDtos;
    }
    /**
     *    méthode qui permet de creer un nouvel Role
     *    @param : Role nvRole
     *    @return : Role
     **/
    @Override
    public Role insertRole(Role nvRole) throws AnomalieException {
        Role savedRole = roleRepository.save(nvRole);
        return savedRole;
    }
    /**
     * méthode qui permet de modifier un Role
     *   @param : int id, RoleDto roleDto
     *   @return : RoleDto
     **/
    @Override
    public RoleDto updateRole(int id, RoleDto roleDto) throws AnomalieException {
        Role roleAModifier = roleRepository.findById(id).orElseThrow(() -> new AnomalieException("Role non trouvé"));

        if (roleDto.getPersonnage().length() < 1) {
            throw new AnomalieException("Le personnage doit avoir un nom contenant au moins 1 lettre");
        }

        roleAModifier.setPersonnage(roleDto.getPersonnage());
        // Vous devrez également gérer la mise à jour des objets Film et Personne si nécessaire.

        return new RoleDto(roleRepository.save(roleAModifier));
    }
    /**
     *    méthode qui supprime un Role à partir de son id
     *    @param : int id
     *    @return : role
     **/

    @Override
    public void deleteRole(int id) {
        roleRepository.deleteById(id);
    }

    /**
     * Methode qui permet de recuperer le role d'une personne à partir de son id
     * @param personneId
     * @return Iterable<RoleDto>
     */
    @Override
    public Iterable<RoleDto> getRolesByPersonneId(int personneId) {
        List<RoleDto> roleDtos = new ArrayList<>();
        Iterable<Role> roles = roleRepository.getRolesByPersonneId(personneId);

        for (Role role : roles) {
            roleDtos.add(new RoleDto(role));
        }

        return roleDtos;
    }
/*
    *//**
     * Methode qui permet de recuperer le role dans un film à partir d'id
     * @param idFilm
     * @return Iterable<RoleDto>
     *//*
    @Override
    public Iterable<Map<String, String>> getRolesByFilmId(int idFilm) {
        List<RoleDto> roleDtos = new ArrayList<>();
        List<Object[]> roles = roleRepository.getRolesByFilm(idFilm);
        for (Object[] role : roles) {
            roleDtos.add(new RoleDto(role));
        }

        return roleDtos;
    }*/

    /**
     * Methode pour retrouver les roles dans des films des personnes à partir des id
     * @param filmId
     * @param personneId
     * @return int
     */
    @Override
    public int countRolesByFilmIdAndPersonneId(int filmId, int personneId) {
        return roleRepository.countRolesByFilmIdAndPersonneId(filmId, personneId);
    }
    @Override
    public Iterable<Map<String, String>> getRolesByFilmId(int idFilm){
        List<Object[]> result = roleRepository.getRolesByFilm(idFilm);
        List<Map<String, String>> responses = new ArrayList<>();
        Set<String> processedActors = new HashSet<>();

        for (Object[] row : result) {
            String personnage = (String) row[0];
            String nomActeur = (String) row[1];
            /*String idImdb = (String) row[2];*/

            if (!processedActors.contains(personnage)) {
                Map<String, String> response = new HashMap<>();
                response.put("personnage", personnage);
                response.put("nomActeur", nomActeur);
                /*response.put("idImdb", idImdb);*/

                responses.add(response);
                processedActors.add(nomActeur);
            }
        }

        return responses;
    }
}

