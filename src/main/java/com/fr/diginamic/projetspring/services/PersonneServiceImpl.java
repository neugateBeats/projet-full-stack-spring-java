package com.fr.diginamic.projetspring.services;


import com.fr.diginamic.projetspring.dto.PersonneDto;
import com.fr.diginamic.projetspring.entites.Personne;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;
import com.fr.diginamic.projetspring.repositories.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PersonneServiceImpl implements PersonneService {


    @Autowired
    private PersonneRepository personneRepository;

    /******************  CRUD  ******************/

    /**
     *
     *    méthode qui permet de retrouver toutes les personnes
     *    @return : personnes
     **/
    public Iterable<PersonneDto> getPersonnes() {
        Iterable<Personne> personnes = personneRepository.findAll();

        List<PersonneDto> dtos = new ArrayList<>();
        for (Personne personne : personnes) {
            dtos.add(new PersonneDto(personne));
        }
        return dtos;
    }

    /**
     * Extraire une personne par son IDImdb
     * @param idImdb
     * @return
     */
    @Override
    public Iterable<Personne> getPersonneImdb(String idImdb) {
        return personneRepository.findPersonneByIdImdb(idImdb);
    }
    /**
           méthode qui permet de retrouver une personne à partir de son id
           @param : int id
            @return : personne
           */

    public PersonneDto getPersonne(int id) {

        Optional<Personne> extractFilm = Optional.of(personneRepository.findById(id).get());
        Personne personne = null;
        if (extractFilm.isPresent()){
            personne = extractFilm.get();
        }
        PersonneDto dto = new PersonneDto(personne);
        return dto;
    }

    /**
        méthode qui permet de creer une nouvelle personne
        @param : Personne nvPersonne
        @return : personne
    */
    public Personne inserer(Personne nvPersonne) throws AnomalieException {
        if (nvPersonne.getidentite().length()==0) {
            throw new AnomalieException("La pesonne doit avoir une identite");
        }
        return personneRepository.save(nvPersonne);
    }

    /**
       méthode qui permet de modifier une personne
       @param : int id, Personne modPersonne
       @return : personne
   */
    public Personne modifier(int id, Personne modPersonne) throws AnomalieException {
        if (modPersonne.getidentite().length() < 1) {
            throw new AnomalieException("L'personne doit avoir un nom contenant au moins 1 lettres");
        }
        Personne personneAModifie = personneRepository.findById(id).get();

        personneAModifie.setIdImdb(modPersonne.getIdImdb());
        personneAModifie.setidentite(modPersonne.getidentite());
        personneAModifie.setDateNaissance(modPersonne.getDateNaissance());
        personneAModifie.setLieuNaissance(modPersonne.getLieuNaissance());
        personneAModifie.setTaille(modPersonne.getTaille());
        personneAModifie.setUrlActeur(modPersonne.getUrlActeur());
        personneAModifie.setUrlRealisateur(modPersonne.getUrlRealisateur());
        personneAModifie.setActeur(modPersonne.getActeur());
        personneAModifie.setRealisateur(modPersonne.getRealisateur());

        return personneRepository.save(personneAModifie);

    }
    /**
         méthode qui supprime une film à partir de son id
         @param : int id
         @return : Iterable<Film>
     */

    public Iterable<Personne> deletePersonneById(int id) {
        personneRepository.deleteById(id);

        Iterable<Personne> Personnes = personneRepository.findAll();
        return Personnes;
    }

    /******************  Autres services  ******************/

    /**
     * Extraire les acteurs communs à 2 films donnés
     * @param idfilm1
     * @param idfilm2
     * @return
     */
    public Iterable<PersonneDto> getActeurCommunsFilms(Integer idfilm1, Integer idfilm2) {
        Iterable<Personne> personnes = personneRepository.findByActeurCommunsFilm(idfilm1, idfilm2);
        List<PersonneDto> dtos = new ArrayList<>();
        for (Personne personne : personnes) {
            dtos.add(new PersonneDto(personne));
        }
        return dtos;
    }
    /**
     * Extraire les acteurs communs à 2 films donnés par idImdb
     * @param idImdbFilm1
     * @param idImdbFilm2
     * @return
     */
    @Override
    public Iterable<PersonneDto> findByActeurCommunsFilmsImdb(String idImdbFilm1, String idImdbFilm2) {
        Iterable<Personne> personnes = personneRepository.findByActeurCommunsFilmsImdb(idImdbFilm1, idImdbFilm2);
        List<PersonneDto> dtos = new ArrayList<>();
        for (Personne personne : personnes) {
            dtos.add(new PersonneDto(personne));
        }
        return dtos;    }
    /**
     * Extraire les acteurs communs à 2 films donnés par nom
     * @param film1
     * @param film2
     * @return
     */
    @Override
    public Iterable<PersonneDto> findActeurCommunA2FilmsNom(String film1, String film2) {
        Iterable<Personne> personnes = personneRepository.findActeurCommunA2FilmsNom(film1, film2);
        List<PersonneDto> dtos = new ArrayList<>();
        for (Personne personne : personnes) {
            dtos.add(new PersonneDto(personne));
        }
        return dtos;
    }


    /**
       méthode qui permet de compter les personnes qui ont le même Id Imdb
       @return : int
   */
    public int countPersonnesByIdImdb(String idImdb) {
        return personneRepository.countPersonnesByIdImdb(idImdb);
    }


    /**
   méthode qui permet d'extraire la personne qui a un Id Imdb
   @return : int
*/
    public Personne personneByIdImdb(String idImdb) {
        return personneRepository.personneByIdImdb(idImdb);
    }


    /**
     * Extraire les acteurs associés au genre dans lequel ils ont le plus joué
     * @return Iterable<Map<String, String>>
     */
    @Override
    public Iterable<Map<String, String>> getPersonnesByGenre(){
        List<Object[]> result = personneRepository.findPersonnesByGenre();
        List<Map<String, String>> responses = new ArrayList<>();
        Set<String> processedActors = new HashSet<>();

        for (Object[] row : result) {
            String nomActeur = (String) row[0];
            String genreAssocie = (String) row[1];

            if (!processedActors.contains(nomActeur)) {
                Map<String, String> response = new HashMap<>();
                response.put("nomActeur", nomActeur);
                response.put("genre", genreAssocie);

                responses.add(response);
                processedActors.add(nomActeur);
            }
        }

        return responses;
    }



}
