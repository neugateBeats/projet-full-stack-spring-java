package com.fr.diginamic.projetspring.services;

import com.fr.diginamic.projetspring.dto.FilmDto;
import com.fr.diginamic.projetspring.dto.PersonneDto;
import com.fr.diginamic.projetspring.entites.Personne;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;
import org.springframework.data.repository.query.Param;

import java.util.Map;

public interface PersonneService {


    /******************  CRUD  ******************/
 Iterable<PersonneDto> getPersonnes();
 Personne inserer(Personne nvPersonne) throws AnomalieException;
 Personne modifier( int id,Personne modPersonne) throws AnomalieException;
 Iterable<Personne> deletePersonneById(int id);


    /******************  Autres services  ******************/

    PersonneDto getPersonne(int id);
    Iterable<PersonneDto> getActeurCommunsFilms(Integer idfilm1, Integer idfilm2);

    Iterable<PersonneDto> findByActeurCommunsFilmsImdb(String idImdbFilm1, String idImdbFilm2);

    Iterable<PersonneDto> findActeurCommunA2FilmsNom(String film1, String film2);

    int countPersonnesByIdImdb(String idImdb);

    Iterable<Personne> getPersonneImdb(String idPersonneImdb);
    Personne personneByIdImdb(String idImdb);

    Iterable<Map<String, String>> getPersonnesByGenre();

}
