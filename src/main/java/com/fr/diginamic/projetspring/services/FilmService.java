package com.fr.diginamic.projetspring.services;

import com.fr.diginamic.projetspring.dto.FilmDto;
import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.exceptions.AnomalieException;

import java.util.Optional;

public interface FilmService {

    /******************  CRUD  ******************/
    Iterable<FilmDto> getFilms();

    FilmDto getFilm(int id);

    Film inserer(Film nvFilm) throws AnomalieException;

    Film modifier(int id, Film modFilm) throws AnomalieException;

    Iterable<Film> deleteFilmById(int id);

    /******************  Autres services  ******************/

    int countFilmsByIdImdb(String idImdb);

    Film filmByIdImdb(String idImdb);

    Iterable<FilmDto> getFilmsByActeurId(int idPersonne) throws AnomalieException;

    Iterable<FilmDto> getAnneeFilm(int startYear, int endYear);

    Iterable<FilmDto> findFilmCommunA2Acteurs(Integer acteurId1, Integer acteurId2);
    Iterable<FilmDto> findFilmCommunA2ActeursImdb(String acteurImdb1, String acteurImdb2);
    Iterable<FilmDto> findFilmCommunA2ActeursNom(String acteurNom1, String acteurNom2);

    Iterable<FilmDto> getFilmsByGenre(Long idGenre) throws AnomalieException;

    Iterable<FilmDto> getFilmsAUnRealisateur(Integer realisateurId);
    Iterable<FilmDto> getFilmsAUnRealisateurImdb(String realisateurImdb);
    Iterable<FilmDto> getFilmsRealisateursContenantChaine(String chaine);


    Iterable<FilmDto> getAnneeFilmByActeur(Integer id, Integer startYear, Integer endYear);
    Iterable<FilmDto> getAnneeFilmByActeurImdb(String acteurImdb, Integer startYear, Integer endYear);
    Iterable<FilmDto> getAnneeFilmByActeurs(String chaine, Integer startYear, Integer endYear);

    Iterable<FilmDto> getFilmsAUnActeur(Integer acteurId);
    Iterable<FilmDto> getFilmsAUnActeurImdb(String acteurImdb);
    Iterable<FilmDto> getFilmsActeursContenantChaine(String chaine);


    Iterable<Film> getFilmImdb(String idFilmImdb);
}


