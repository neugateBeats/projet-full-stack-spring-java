package com.fr.diginamic.projetspring.traitementFichiers;

import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.entites.Personne;
import com.fr.diginamic.projetspring.entites.Role;
import com.fr.diginamic.projetspring.services.FilmService;
import com.fr.diginamic.projetspring.services.PersonneService;
import com.fr.diginamic.projetspring.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import static java.nio.file.Files.readAllLines;

@Component
public class FichierRole {

    @Autowired
    private RoleService roleService;
    @Autowired
    private FilmService filmService;
    @Autowired
    private PersonneService personneService;
    /**
     * Methode qui lit le fichier roles.csv pour remplir la table role et films_personnes
     * @throws Exception
     **/
    @Transactional
    public void traitementFichierRole() throws Exception {
        String fileName = "roles.csv";
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("File not found: " + fileName);
        } else {
            Path pathFile = Paths.get(Objects.requireNonNull(resource).toURI());
            List<String> lignes = readAllLines(pathFile);
            lignes.remove(0);
            for (String ligne : lignes) {
                String[] elements = ligne.split(";");
                if (elements.length >= 3) {
                    System.out.println(Arrays.toString(elements));
                    String idFilmImdb = elements[0];
                    Iterable<Film> films = filmService.getFilmImdb(idFilmImdb);
                    Iterator<Film> filmIterator = films.iterator();
                    if (filmIterator.hasNext()) {
                        Film film = filmIterator.next();
                        String idPersonneImdb = elements[1];
                        Iterable<Personne> personnes = personneService.getPersonneImdb(idPersonneImdb);
                        Iterator<Personne> personneIterator = personnes.iterator();
                        if (personneIterator.hasNext()) {
                            Personne personne = personneIterator.next();
                            String personnage = elements[2].trim();
                            if (!personnage.isEmpty()) {
                                Role nvRole = new Role();
                                nvRole.setPersonnage(personnage);
                                nvRole.setFilm(film);
                                nvRole.setActeur(personne);
                                roleService.insertRole(nvRole);
                            } else {
                                System.out.println("Personnage vide ignoré.");
                            }
                        } else {
                            System.out.println("Aucune personne trouvée pour le rôle.");
                        }
                    } else {
                        System.out.println("Aucun film trouvé pour le rôle.");
                    }
                } else {
                    System.out.println("La ligne ne contient pas assez d'éléments.");
                }
            }
        }
    }
}
