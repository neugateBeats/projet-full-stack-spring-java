package com.fr.diginamic.projetspring.traitementFichiers;

import com.fr.diginamic.projetspring.entites.Personne;
import com.fr.diginamic.projetspring.services.PersonneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static java.nio.file.Files.readAllLines;

@Component
public class FichierPersonne {


    @Autowired
    private PersonneService personneService;

    /**
     * methode qui lit le fichier passé en parametre (acteurs.csv ou realisateurs.csv) pour remplir la table personne
     * @throws Exception
     **/
    @Transactional
    public void traitementFichierPersonne(String fileName) throws Exception {
        //récupération du fichier dans les ressources
        //String fileName = "acteurs.csv";
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file not found : " + fileName);
        } else {
            Path pathFile = Paths.get(Objects.requireNonNull(resource).toURI());

            System.out.println("File " + pathFile + " exists : " + Files.exists(pathFile)
                    + " regularFile : " + Files.isRegularFile(pathFile)
                    + " directory : " + Files.isDirectory(pathFile));

            // Ouverture d'un stream vers le fichier
            List<String> lignes = readAllLines(pathFile);
            lignes.remove(0);

            int index = 0;
            for (String ligne : lignes) {
                String[] elements = ligne.split(";");
                Personne nvPersonne = new Personne();

                //Récupèration des informations de personne
                nvPersonne.setIdImdb(elements[0]);
                //System.out.println(nvPersonne.getIdImdb());

                // Si la personne n'existe pas, i l faut l'insère dans personne
                if (personneService.countPersonnesByIdImdb(nvPersonne.getIdImdb()) == 0) {
                    nvPersonne.setidentite(elements[1]);

                    String strDate = elements[2].trim();
                    if (!strDate.isEmpty()) {
                        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd yyyy", Locale.ENGLISH);
                        try {
                            Date date = sdf.parse(strDate);
                            nvPersonne.setDateNaissance(date);
                        } catch (ParseException e) {
                            System.out.println(nvPersonne.getIdImdb()+ " : "+e);
                        }
                    }
                    nvPersonne.setLieuNaissance(elements[3]);

                    if (fileName.equals("acteurs.csv")) {
                        nvPersonne.setActeur(true);
                        nvPersonne.setTaille(elements[4]);
                        nvPersonne.setUrlActeur(elements[5]);
                    } else if ((fileName.equals("realisateurs.csv"))) {
                        nvPersonne.setRealisateur(true);
                        nvPersonne.setUrlRealisateur(elements[4]);
                    }
                    Personne personneAjoute = personneService.inserer(nvPersonne);

                } else {

                    Personne personneAModifier = personneService.personneByIdImdb(nvPersonne.getIdImdb());

                    if (fileName.equals("acteurs.csv") && (personneAModifier.getActeur()==null)) {
                        //System.out.println(nvPersonne.getIdImdb() + " existe déjà");
                        personneAModifier.setActeur(true);
                        personneAModifier.setTaille(elements[4]);
                        personneAModifier.setUrlActeur(elements[5]);
                    } else if ((fileName.equals("realisateurs.csv")) && (personneAModifier.getRealisateur()== null)) {
                        //System.out.println(nvPersonne.getIdImdb() + " existe déjà");
                        personneAModifier.setRealisateur(true);
                        personneAModifier.setUrlRealisateur(elements[4]);
                    }
                    Personne personneModifie = personneService.modifier(personneAModifier.getId(), personneAModifier);
                }
                /*
                index++;
                if (index > 5000) {
                    break;
                }*/
            }


        }
    }
}
