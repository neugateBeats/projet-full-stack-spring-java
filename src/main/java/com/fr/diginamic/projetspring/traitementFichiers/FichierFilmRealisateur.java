package com.fr.diginamic.projetspring.traitementFichiers;

import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.entites.Personne;
import com.fr.diginamic.projetspring.services.FilmService;
import com.fr.diginamic.projetspring.services.GenreService;
import com.fr.diginamic.projetspring.services.PersonneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static java.nio.file.Files.readAllLines;


@Component
public class FichierFilmRealisateur {

    @Autowired
    private GenreService genreService;
    @Autowired
    private FilmService filmService;
    @Autowired
    private PersonneService personneService;

    /**
     * methode qui lit le fichier film_realisateurs.csv pour remplir la table films_personnes
     * @throws Exception
     **/
    @Transactional
    public void traitementFichierFilmRealisateur() throws Exception {
        //récupération du fichier dans les ressources
        String fileName = "film_realisateurs.csv";
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file not found : " + fileName);
        } else {
            Path pathFile = Paths.get(Objects.requireNonNull(resource).toURI());

            System.out.println("File " + pathFile + " exists : " + Files.exists(pathFile)
                    + " regularFile : " + Files.isRegularFile(pathFile)
                    + " directory : " + Files.isDirectory(pathFile));

            // Ouverture d'un stream vers le fichier
            List<String> lignes = readAllLines(pathFile);
            lignes.remove(0);
            int index = 0;
            for (String ligne : lignes) {
                String[] elements = ligne.split(";");
                //System.out.println(elements[0] + "  " + elements[1]);
                // Récupèration de Id du film à partir de Id Imdb du film dans fichier
                Film filmAModifie = filmService.filmByIdImdb(elements[0]);

                System.out.println();
                // Récupèration de Id de la personne (réalisateur) à partir de Id Imdb du réalisateur dans fichier
                Personne personneTrouve = personneService.personneByIdImdb(elements[1]);

                if (personneTrouve != null) {
                    filmAModifie.getPersonnes().add(personneTrouve);
                }
            }
        }
    }
}

