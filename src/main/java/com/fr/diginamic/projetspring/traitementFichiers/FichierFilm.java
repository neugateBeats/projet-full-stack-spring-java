package com.fr.diginamic.projetspring.traitementFichiers;

import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.entites.Genre;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import com.fr.diginamic.projetspring.entites.Film;
import com.fr.diginamic.projetspring.entites.Genre;
import com.fr.diginamic.projetspring.services.FilmService;
import com.fr.diginamic.projetspring.services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static java.nio.file.Files.readAllLines;


@Component
public class FichierFilm  {

    @Autowired
    private GenreService genreService;
    @Autowired
    private FilmService filmService;

    /**
     * methode qui lit le fichier films.csv pour remplir les tables film, genre et film_genre
     * @throws Exception
     **/
    @Transactional
    public void traitementFichierFilm() throws Exception {
        //récupération du fichier dans les ressources
        String fileName = "films.csv";
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file not found : " + fileName);
        } else {
            Path pathFile = Paths.get(Objects.requireNonNull(resource).toURI());

            System.out.println("File " + pathFile + " exists : " + Files.exists(pathFile)
                    + " regularFile : " + Files.isRegularFile(pathFile)
                    + " directory : " + Files.isDirectory(pathFile));

            // Ouverture d'un stream vers le fichier
            List<String> lignes = readAllLines(pathFile);
            lignes.remove(0);
            for (String ligne : lignes) {
                String[] elements = ligne.split(";");

                Film nvFilm = new Film();

                //Récupèration des informations du film
                nvFilm.setIdImdb(elements[0]);
                nvFilm.setNom(elements[1]);

                SimpleDateFormat formateur = new SimpleDateFormat("yyyy");
                nvFilm.setAnnee(formateur.parse(elements[2]));

                if (!elements[3].isEmpty()) {
                    nvFilm.setRating(Float.parseFloat(elements[3].replaceAll(",", ".")));
                }
                nvFilm.setUrl(elements[4]);
                nvFilm.setLieuTournage(elements[5]);

                nvFilm.setLangue(elements[7]);
                if (elements.length > 8) {
                    nvFilm.setResume(elements[8]);
                }
                if (elements.length > 9) {
                    nvFilm.setPays(elements[9]);
                }
                if (elements.length > 10) {
                    nvFilm.setPays(elements[elements.length-1]);
                }

                // Si la film n'existe pas, on l'insère dans film
                if (filmService.countFilmsByIdImdb(nvFilm.getIdImdb()) == 0) {
                    Film filmAjoute = filmService.inserer(nvFilm);

                    //Récupération des genres
                    if (!elements[6].isEmpty()) {

                        String nomGenres = elements[6];
                        String[] genresElements = nomGenres.split(",");

                        Set<Genre> genres = new HashSet<>();

                        for (String genreElement : genresElements) {

                            Genre nvGenre = new Genre();
                            nvGenre.setNom(genreElement);
                            // Si le genre n'existe pas, on l'insère dans genre
                            if (genreService.countGenreByName(genreElement) == 0) {
                                System.out.println("genre " + nvGenre.getNom() + " n'existe pas, on l'insere");
                                genreService.inserer(nvGenre);
                            }
                            // Récupèration du genre ajouté
                            Genre genre;
                            genre = genreService.findByName(nvGenre.getNom());
                            if (genre != null) {
                               // System.out.println("genre Id : "+genre.getIdGenre());
                                genres.add(genre);
                                // remplir la table film_genre
                                 filmAjoute.setGenres(genres);
                                Film filmModifie = filmService.modifier(filmAjoute.getId(),filmAjoute);
                            }
                        }
                    }
                } else {
                   System.out.println(nvFilm.getIdImdb() + " existe déjà");
                }
            }
        }
    }
}

