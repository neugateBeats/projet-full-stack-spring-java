package com.fr.diginamic.projetspring.exceptions;

public class AnomalieException extends Exception {

    public AnomalieException(String message) {
        super(message);
    }
}
